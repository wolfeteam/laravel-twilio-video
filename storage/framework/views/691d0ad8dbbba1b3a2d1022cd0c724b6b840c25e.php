
<?php $__env->startSection('title'); ?>
<title>Telemed | Edit Patient</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('menu'); ?>
<li><a href="<?php echo e(url('home')); ?>" >HOME</a></li>
<li class="active"><a href="<?php echo e(url('viewpatient')); ?>">PATIENTS</a></li>
<li><a href="<?php echo e(url('callpatient')); ?>">CALL</a></li>
<li><a  href="<?php echo e(url('setup')); ?>">SETUP</a></li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<script>
   var loadFile = function(event) {
     var output = document.getElementById('output');
     output.src = URL.createObjectURL(event.target.files[0]);
   };
</script>
<div class="container-fluid">
   <div class="row">
      <div class="col-md-3">
         <ul class="list-group text-right">
            <li class="list-group-item "><a href="<?php echo e(url('viewpatient')); ?>" >View Patients</a></li>
            <li class="list-group-item"><a href="<?php echo e(url('addpatientpage')); ?>" class="left-nav-active">Edit Patient</a></li>
            <!--<li class="list-group-item"><a href="">Edit Patient Data</a></li>-->
         </ul>
      </div>
      <div class="col-md-7 lft-part">
         <div class="row">
            <div class="col-md-12">
               <div class="row">
                  <?php if(session('status')): ?>
                  <div id="alertmsg" class="alert alert-success">
                     <?php echo e(session('status')); ?>

                  </div>
                  <?php endif; ?>
                  <form class="form-signin" id="register-form" method="post" enctype="multipart/form-data" action="<?php echo e(url('editpatient')); ?>">
                     <h2 class="form-signin-heading">Edit Patient</h2>
                     <div class="col-md-12">
                        <div class="row">
                           <div class="row">
                              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                              <input type="hidden" name="patientid" value="<?php echo e($patient['patientid']); ?>">
                              
                              <div class="col-md-6">
                                 <div class="col-md-12">
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>First Name *</label>
                                       <input type="text" class="form-control" value="<?php echo e($patient['firstname']); ?>" placeholder="First Name *" name="First"   required />
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>Last Name *</label>
                                       <input type="text" class="form-control" value="<?php echo e($patient['lastname']); ?>" placeholder="Last Name *"  name="Last"  required  />
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <br>
                                    <div class="form-group radio-label">
                                       <label>Sex</label>            
                                       <input class="custom-radio" type="radio" checked="checked" <?php if($patient["sex"] =='M'): ?>  checked="checked" <?php endif; ?> name="sex" id="3" value="Male">
                                       <label for="3"> Male</label>
                                       <input class="custom-radio" <?php if($patient["sex"] =='F'): ?>  checked="checked" <?php endif; ?> type="radio" name="sex" id="4" value="Female">
                                       <label for="4"> Female</label>
                                    </div>
                                    <br>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group"> 
                                          <img src="<?php echo e(url('images/noimage.jpg')); ?>}}" id="output" alt="" class="pic-pp"> 
                                       </div>
                                    </div>
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <!--<input type="file" name="image" id="image">-->
                                          <label class="btn btn-primary btn-cus"> Upload Photo
                                          <input type="file"  onchange="loadFile(event)"  name="photo" id="image" style="display: none;">
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label>Country *</label>
                                 <select class="form-control"  name="Country"  required>
                                    <option value="" selected="selected"  >-- Select Country -- </option>
                                    <option  <?php if($patient['countrycode'] =="USA"): ?> selected <?php endif; ?>  value="USA">United States</option> 
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>Mobile Phone</label>
                                 <input type="number" class="form-control"  value="<?php echo e(array_key_exists('mobilephone',$patient)?$patient['mobilephone']:''); ?>"  placeholder="Mobile number *" name="mobilephone"   required />
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>PhoneCarrier *</label>
                                 <select class="form-control" id="PhoneCarrier" name="mobilecarrierid" required>
                                    <option value="">-- Select Phone Carrier --</option>
                                    <option value="21" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="21"): ?> selected <?php endif; ?>>ACS Wireless</option>
                                    <option value="22" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:"" =="22"): ?> selected <?php endif; ?>>Alltel</option>
                                    <option value="24" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="24"): ?> selected <?php endif; ?>>AT&T Cingular</option>
                                    <option value="26" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="26"): ?> selected <?php endif; ?>>Boost (iDEN)</option>
                                    <option value="34" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="34"): ?> selected <?php endif; ?>>Cincinnati Bell</option>
                                    <option value="35" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="35"): ?> selected <?php endif; ?>>Cricket Communincations</option>
                                    <option value="81" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="81"): ?> selected <?php endif; ?>>MetroPCS</option>
                                    <option value="58" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="58"): ?> selected <?php endif; ?>>SaskTel Mobility</option>
                                    <option value="36" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="36"): ?> selected <?php endif; ?>>GCI Communications</option>
                                    <option value="38" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="38"): ?> selected <?php endif; ?>>Illinois Valley Cellular</option>
                                    <option value="37" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="37"): ?> selected <?php endif; ?>>Golden State Cellular</option>
                                    <option value="59" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="59"): ?> selected <?php endif; ?>>Telus Mobility</option>
                                    <option value="49" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="49"): ?> selected <?php endif; ?>>US Cellular</option>
                                    <option value="50" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="50"): ?> selected <?php endif; ?>>Verizon Wireless</option>
                                    <option value="60" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="60"): ?> selected <?php endif; ?>>Virgin Mobile</option>
                                    <option value="57" <?php if(array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="57"): ?> selected <?php endif; ?>>Rogers Wireless</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>EMailAddress</label>
                                 <input type="text" class="form-control"  value="<?php echo e(array_key_exists('email',$patient)?$patient['email']:''); ?>"  placeholder="Email Address" name="email"    />
                              </div>
                           </div>
                           <div class="col-md-6 ">
                              <div class="form-group">
                                 <label>Birthdate</label>
                                 <?php
                                    $dt1 = new DateTime($patient['dob']);
                                    $Birthdate = $dt1->format('Y-m-d');
                                    
                            
                                    
                                    ?>
                                 <input type="date" class="form-control"   value="<?php echo e($Birthdate); ?>"  name="dob"   id="Birthdate" />
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Profile Notes</label>
                                 <textarea class="form-control"   value="" placeholder="Profile Notes" id="ProfileNotes" name="ProfileNotes"   ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Alert Notes </label>
                                 <textarea class="form-control"   value="" placeholder="Alert Notes" id="AlertNotes" name="AlertNotes"  ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Appointment Notes </label>
                                 <textarea class="form-control"   value="" placeholder="Appointment Notes" id="AppointmentNotes" name="AppointmentNotes"   ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Billing Notes</label>
                                 <textarea class="form-control"  value=""  placeholder="Billing Notes" id="BillingNotes" name="BillingNotes"   ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Reg Note</label>
                                 <textarea class="form-control"  value=""  placeholder="Reg Note" id="RegNote" name="RegNote"   ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <button type="submit" class="btn btn-default btn-green" name="btn-signup">Update Patient Data </button>
                              </div>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>