

<?php $__env->startSection('title'); ?>
    <title>Telemed | Add Patient</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
<li><a href="  <?php echo e(url('home')); ?>" >HOME</a></li>
<li class="active"><a href="<?php echo e(url('viewpatient')); ?> ">PATIENTS</a></li>
<li><a href="<?php echo e(url('callpatient')); ?> ">CALL</a></li>
<li><a  href="<?php echo e(url('setup')); ?>">SETUP</a></li>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>


<div class="container-fluid"> 
  <div class="row">
    <div class="col-md-3">
      <ul class="list-group text-right">
        <li class="list-group-item "><a href="<?php echo e(url('viewpatient')); ?> " >View / Edit Patients</a></li>
        <li class="list-group-item"><a href="<?php echo e(url('addpatientpage')); ?>" >Add Patient</a></li>
        <!--<li class="list-group-item" class="left-nav-active"><a href="">Edit Patient Data</a></li>-->
      </ul>
    </div>
    <div class="col-md-7 lft-part">
      <div class="row">
        <div class="col-md-12">
           
            <h2 class="form-signin-heading">Patient Details</h2>
     <hr />
      <?php if(session('status')): ?>
                      <div id="alertmsg" class="alert alert-success">
                          <?php echo e(session('status')); ?>

                      </div>
                      <?php endif; ?>
            <table class="table table-bordered table-responsive">
              <tbody>
              <tr>
                <td><b>Prefix</b></td><td><?php echo e($patient->Prefix); ?></td>
                <td><b>F-name</b></td><td><?php echo e($patient->First); ?></td>
                <td class="text-center" colspan="2" rowspan="5"> <img width="200px" src="<?php echo e(url('')); ?>/<?php echo e($patient->Picture); ?>"> </td>
              </tr>

              <tr>
                 <td><b>M-name</b></td><td><?php echo e($patient->Middle); ?></td>
                <td><b>L-name</b></td><td><?php echo e($patient->Last); ?></td>
                
              </tr>

              <tr>
                <td><b>Suffix</b></td><td><?php echo e($patient->Suffix); ?></td>
                <td><b>Nickname</b></td><td><?php echo e($patient->Nickname); ?></td>
               
              </tr>

              <tr>
                 <td><b>Address1</b></td><td><?php echo e($patient->Address1); ?></td>
                <td><b>Address2</b></td><td><?php echo e($patient->Address2); ?></td>
                
              </tr>

              <tr>
                <td><b>City</b></td><td><?php echo e($patient->City); ?></td>
                <td><b>State</b></td><td><?php echo e($patient->State); ?></td>
              </tr>

              <tr>
                <td><b>Zip</b></td><td><?php echo e($patient->Zip); ?></td>
                <td><b>Country</b></td><td><?php echo e($patient->Country); ?></td>
                <td><b>AddressType</b></td><td><?php echo e($patient->AddressType); ?></td>
              </tr>

              <tr>
                <td><b>County</b></td><td><?php echo e($patient->County); ?></td>
                <td><b>Phone1</b></td><td><?php echo e($patient->Phone1); ?></td>
                <td><b>Phone1Type</b></td><td ><?php echo e($patient->Phone1Type); ?></td>
              </tr>

              <tr>
                <td><b>Phone2</b></td><td><?php echo e($patient->Phone2); ?></td>
                <td><b>Phone2Type</b></td><td colspan="3"><?php echo e($patient->Phone2Type); ?></td>
               
              </tr>

              <tr>
                 <td><b>Phone3</b></td><td><?php echo e($patient->Phone3); ?></td>
                 <td><b>Phone3Type</b></td><td colspan="3" ><?php echo e($patient->Phone3Type); ?></td>
              </tr>

              <tr>
                <td><b>EMailAddress</b></td><td colspan="5"><?php echo e($patient->EMailAddress); ?></td>
              </tr>

              <tr>
                <td><b>AlternateAddress1</b></td><td><?php echo e($patient->AlternateAddress1); ?></td>
                <td><b>AlternateAddress2</b></td><td ><?php echo e($patient->AlternateAddress2); ?></td>
                <td><b>AlternateCity</b></td><td><?php echo e($patient->AlternateCity); ?></td>
              </tr>

              <tr>
                <td><b>AlternateState</b></td><td ><?php echo e($patient->AlternateState); ?></td>
                <td><b>AlternateZip</b></td><td><?php echo e($patient->AlternateZip); ?></td>
                <td><b>AlternateCounty</b></td><td ><?php echo e($patient->AlternateCounty); ?></td>
              </tr>

              <tr>
                <td><b>AlternateCountry</b></td><td><?php echo e($patient->AlternateCountry); ?></td>
                <td><b>AlternateAddressType</b></td><td ><?php echo e($patient->AlternateAddressType); ?></td>
                <td><b>SchoolName</b></td><td><?php echo e($patient->SchoolName); ?></td>
              </tr>

              <tr>
                <td><b>SSN</b></td><td ><?php echo e($patient->SSN); ?></td>
                <td><b>Birthdate</b></td><td><?php echo e($patient->Birthdate); ?></td>
                <td><b>DeathDate</b></td><td><?php echo e($patient->DeathDate); ?></td>
              </tr>

              <tr>
                <td><b>Sex</b></td><td><?php echo e($patient->Sex); ?></td>
                <td><b>ReferredByPatientId</b></td><td ><?php echo e($patient->ReferredByPatientId); ?></td>
                <td><b>PatientSameAsGuarantor</b></td><td><?php if($patient->PatientSameAsGuarantor==1): ?> Yes <?php else: ?> No <?php endif; ?> </td>
              </tr>

               <tr>
                <td><b>MedicalRecordNumber</b></td><td ><?php echo e($patient->MedicalRecordNumber); ?></td>
                <td><b>Visit document num</b></td><td><?php echo e($patient->visdocnum); ?></td>
                <td><b>SSDID</b></td><td ><?php echo e($patient->SSDID); ?></td>
              </tr>

              <tr>
                 <td><b>ProfileNotes</b></td><td colspan="5"><?php echo e($patient->ProfileNotes); ?></td>
              </tr>

              <tr>
                 <td><b>AlertNotes</b></td><td colspan="5"><?php echo e($patient->AlertNotes); ?></td>
              </tr>

              <tr>
                <td><b>AppointmentNotes</b></td><td colspan="5"><?php echo e($patient->AppointmentNotes); ?></td>
              </tr>

              <tr>
                <td><b>BillingNotes</b></td><td  colspan="5"><?php echo e($patient->BillingNotes); ?></td>
              </tr>

              <tr>
                <td><b>RegNote</b></td><td  colspan="5" ><?php echo e($patient->RegNote); ?></td>
              </tr>

              <tr>
                <td><b>Created</b></td><td ><?php echo e($patient->Created); ?></td>
                <td><b>LastModified</b></td><td ><?php echo e($patient->LastModified); ?></td>
              </tr>
            </tbody>
            </table>

     <hr />

      <form action="<?php echo e(url('uppatientfiles')); ?>" class="form-signin" id="register-form" method="post" enctype="multipart/form-data">
                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                 <input type="hidden" name="patientid" value="<?php echo e(@$patient->PatientProfileId); ?>">

            <div class="col-md-6 ll-lft">
 
                        <div class="row">
            <div class="row">
              
                <div class="col-md-12">
                    <div class="form-group">
                      <label>Data Upload Type</label>
                     </div>
                  </div>
                  <div class="col-md-12 text-right rr">
                    <div class="form-group">
                      <!--<span>
                      Image <input type="radio" class="form-control1" name="RecevedTextMessage" value="Yes" >
                      </span>--> 
                      <span>Image</span><input class="custom-radio" type="radio" name="Type" id="image" checked="checked" value="Image"><label for="image"> <span>Image</span></label>
                       </div>
                  </div>
                  <div class="col-md-12 text-right rr">
                    <div class="form-group">
                     <!-- <span>
                      Video <input type="radio" class="form-control1" name="RecevedTextMessage" value="Yes" >
                      </span>-->
                      <span>Video</span><input class="custom-radio" type="radio" name="Type" id="video" value="Video"><label for="video"><span> Video</span></label>
                        </div>
                  </div>
                  <div class="col-md-12 text-right rr">
                    <div class="form-group">
                     <!-- <span>
                      Document <input type="radio" class="form-control1" name="RecevedTextMessage" value="Yes" >
                      </span> -->
                      <span>Document</span><input class="custom-radio" type="radio" name="Type" id="document" value="Document"><label for="document"> <span>Document</span></label> </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                    <br/>
                     <label class="btn btn-primary btn-cus"> Upload Documents
                            <input type="file" id="file" name="file" style="display: none;">
                          </label> </div>

                            <div class="form-group"> 
                          <!--<input type="file" name="image" id="image">-->
                         
                           <button onclick="return chkfile()" type="submit" class="btn btn-default btn-green" name="btn-submit"> Submit Documents</button>
                     
                        </div>
                  </div>
                  
                  
                  
                  
                  
                  <div class="col-md-12">
                  
                  </div>
    
                 
            </div>
            </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12 pull-right text-right">
                    <div class="row">


                      <div class="col-md-12">
                        
                            <div class="form-group">
                  <div class="patient-docu">

                    <h5>Patient Document</h5>
                      
                        
                                        </div>
                                        </div>
                       
                      </div>
                    </div>
                  </div>
                </div>
              </div>
      </form>
      </div>
    </div>


                     


  </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">
  function chkfile(){
    if(document.getElementById("file").files.length == 0 ){
      alert('please select patient document and upload it');
     return false;
    }
    else{
      return true;
    }
  }
</script>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>