

<?php $__env->startSection('title'); ?>
<title>Telemed | Home</title>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('menu'); ?>
<li class="active"><a href="home" >HOME</a></li>
<li><a href="viewpatient">PATIENTS</a></li>
<li><a href="callpatient">CALL</a></li>
<li><a  href="setup">SETUP</a></li>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<br><br>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <ul class="list-group text-right">
                <li class="list-group-item "><a href="home" class="left-nav-active">Home</a></li>
             </ul>          
        </div>

        <div class="col-md-7">


            <div class="row">
     <?php if(session('status')): ?>
    <div id="alertmsg" class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
    <?php endif; ?>
    
        <div class="col-md-6 ">
            <div class="panel panel-default">
               

                <div class="panel-body">
                    
                    <div class="midSection">
        <div class="mid"><h2 class="text_top form-signin-heading">Welcome to HHM Telemed.</h2>
        <p> 
            The future of healthcare. Inside this app, you can setup virtual calls with patients and share their data right there with them, all full screen and in real-time. <br>
            The goal is to ensure the best medical care anywhere in the world.Sincerely,</p>
          <img src="images/dr signature.png" width="200"  alt=""/><br>Dr. Ronald Sam, MD
          </div>
          </div>
            </div>
        </div>

            </div>  
            <div class="col-md-6">
                    <img src="/images/doctor.jpg" alt="" class="img-responsive">
                </div>
                
            
        </div>
    </div>
</div>
</div>

<?php $__env->stopSection(); ?>







<?php echo $__env->make('app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>