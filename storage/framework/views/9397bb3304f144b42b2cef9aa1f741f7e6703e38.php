<?php $__env->startSection('title'); ?>
<title>Telemed | Call Patient</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
<?php if(Auth::user()): ?>
<li><a href="home" >HOME</a></li>
<li ><a href="viewpatient">PATIENTS</a></li>
<li class="active"><a href="callpatient">CALL</a></li>
<li ><a href="setup">SETUP</a></li>
<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header-script'); ?>
<script src="//media.twiliocdn.com/sdk/js/video/v1/twilio-video.min.js"></script>
<script>
    Twilio.Video.createLocalTracks({
       audio: true,
       video: { /*width: 300*/ }
    }).then(function(localTracks) {
       return Twilio.Video.connect('<?php echo e($accessToken); ?>', {
           name: '<?php echo e($roomName); ?>',
           tracks: localTracks,
           video: { /*width: 300*/ }
       });
    }).then(function(room) {
       console.log('Successfully joined a Room: ', room.name);

       room.participants.forEach(participantConnected);

       var previewContainer = document.getElementById(room.localParticipant.sid);
       if (!previewContainer || !previewContainer.querySelector('video')) {
           participantConnected(room.localParticipant);
       }

       room.on('participantConnected', function(participant) {
           console.log("Joining: '" +  participant.identity +  "'");
           participantConnected(participant);
       });

       room.on('participantDisconnected', function(participant) {
           console.log("Disconnected: '"  + participant.identity +  "'");
           participantDisconnected(participant);
       });
    });
    // additional functions will be added after this point
    function participantConnected(participant) {
       console.log('Participant "%s" connected', participant.identity);
    
       const div = document.createElement('div');
       div.id = participant.sid;
       div.setAttribute("style", "float: left; margin: 10px;");
       div.innerHTML = "<div style='clear:both'>"+ participant.identity+ "</div>";
    
       participant.tracks.forEach(function(track) {
           trackAdded(div, track)
       });
    
       participant.on('trackAdded', function(track) {
           trackAdded(div, track)
       });
       participant.on('trackRemoved', trackRemoved);
    
       document.getElementById('media-div').appendChild(div);
    }

function participantDisconnected(participant) {
   console.log('Participant "%s" disconnected', participant.identity);

   participant.tracks.forEach(trackRemoved);
   document.getElementById(participant.sid).remove();
}
function trackAdded(div, track) {
   div.appendChild(track.attach());
   var video = div.getElementsByTagName("video")[0];
   if (video) {
    //   video.setAttribute("style", "max-width:300px;");
   }
}

function trackRemoved(track) {
   track.detach().forEach( function(element) { element.remove() });
}
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="content">
   <div class="title m-b-md">
       Video Chat Rooms
   </div>
    <div class="row">
        <div class="col-md-4"></div>
        <div id="media-div">
    </div>        
   </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.public', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>