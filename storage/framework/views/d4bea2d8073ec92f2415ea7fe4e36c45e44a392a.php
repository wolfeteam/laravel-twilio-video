<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Telemed | Registration</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/login.css">
     <script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>


</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">         
            <img src="/images/logo.png" alt="HHM TELEMEDICINE" class="img-responsive">
        </div>
    </div>
        <form class="form-signin" method="post" id="register-form"  action="<?php echo e(route('register')); ?>" enctype="multipart/form-data"/>
                        <?php echo e(csrf_field()); ?>


    <div class="row">
        <div class="col-md-4 text-center intro intros">
            <br/>
                                    <img src="images/image_placeholder.png" id="output" alt="" class="img-responsive center-block profile-circle"> 
                          
                 <br/>
              <label class="btn btn-primary" name="btn-signup" style="color: #FFFFFF!important;">Upload Photo
             <input type="file" name="image" id="image"  onchange="loadFile(event)"  style="display: none;">
            </label>
        </div>
        <div class="col-md-4">
            <div class="form lft-part">
            
        <h2 class="form-signin-heading">Telemed Registration</h2>
                     <div class="form-group">
        <input type="text" class="form-control" placeholder="Title " id="title" name="title"   />
                                <?php if($errors->has('title')): ?>
                                <div class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                                    <span class="help-block ">
                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                    </span> 
                                </div>
                                <?php endif; ?>
                            
        </div>
         <div class="form-group">
        <input type="text" class="form-control" placeholder="Name *" id="name" name="name"   />
                        <?php if($errors->has('name')): ?>
                                <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <span class="help-block ">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span> 
                                </div>
                                <?php endif; ?>

        </div>
        <div class="form-group">
        <input type="email" class="form-control" placeholder="Email *" id="email" name="email"   />
                        <?php if($errors->has('email')): ?>
                                <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                    <span class="help-block ">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span> 
                                </div>
                                <?php endif; ?>
        <span id="check-e"></span>
        </div>
         <div class="form-group">
        <input type="text" class="form-control" placeholder="Hospitals Location" id="hospital" name="hospital"   />
                        <?php if($errors->has('hospitals')): ?>
                                <div class="form-group<?php echo e($errors->has('hospitals') ? ' has-error' : ''); ?>">
                                    <span class="help-block ">
                                        <strong><?php echo e($errors->first('hospitals')); ?></strong>
                                    </span> 
                                </div>
                                <?php endif; ?>
        </div>

        <div class="form-group">
        <input type="text" class="form-control" placeholder="Username *" id="username" name="username"   />
                        <?php if($errors->has('username')): ?>
                                <div class="form-group<?php echo e($errors->has('username') ? ' has-error' : ''); ?>">
                                    <span class="help-block ">
                                        <strong><?php echo e($errors->first('username')); ?></strong>
                                    </span> 
                                </div>
                                <?php endif; ?>
        </div>

         <div class="form-group">
        <input type="password" class="form-control" placeholder="Password *" id="password" name="password"   />
                        <?php if($errors->has('password')): ?>
                                <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                    <span class="help-block ">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span> 
                                </div>
                                <?php endif; ?>
        </div>
         <div class="form-group">
        <input type="password" class="form-control" placeholder="Confirm Password *" name="password_confirmation"   />
                        <?php if($errors->has('password_confirmation')): ?>
                                <div class="form-group<?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
                                    <span class="help-block ">
                                        <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                                    </span> 
                                </div>
                                <?php endif; ?>
        </div>
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary pull-right" name="btn-signup">
            Register
            </button> 
            <a href="login" class="btn btn-default regbtn pull-right">Log In Here</a>
        </div> 
      

            </div>
        </div>
    </div> 
     </form>
</div>

<div class="container-fluid">   
    <div class="row">
        <div class="col-md-12">         
            <br/>
            <img src="/images/circle-line-left.png" alt="" class="pull-left  img-responsive" >
        </div>
    </div>  
</div>

   <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
</body>
</html>