<?php $__env->startSection('title'); ?>
<title>Telemed | View Patients</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('menu'); ?>
<li><a href="<?php echo e(URL('home')); ?>" >HOME</a></li>
<li class="active"><a href="viewpatient">PATIENTS</a></li>
<li><a href="callpatient">CALL</a></li>
<li><a  href="<?php echo e(url('setup')); ?>">SETUP</a></li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
<div class="row">
   <div class="col-md-3">
        <ul class="list-group text-right">
            <li class="list-group-item "><a href="viewpatient" class="left-nav-active">View / Edit Patients</a></li>
            <li class="list-group-item"><a href="addpatientpage" >Add Patient</a></li>
            <!--<li class="list-group-item"><a href="">Edit Patient Data</a></li>-->
        </ul>
   </div>
   <div class="col-md-7">
      <div class="row">
         <div class="col-md-6">
            <form method="post" action="<?php echo e(url('searchpatient')); ?>">
               <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
               <div class="form-group input-group">                    
                  <input type="text" name="search"  class="form-control search" placeholder="Search..." >
                  <span class="input-group-btn"> 
                  <button  class="btn btn-default" type="submit">
                  <i class="fa fa-search"></i>
                  </button>
                  </span>
               </div>
            </form>
         </div>
      </div>
      
      <?php if(session('status')): ?>
      <div id="alertmsg" class="alert alert-success">
         <?php echo e(session('status')); ?>

      </div>
      <?php endif; ?>
      <div class="row"  id="txtHint">
         <?php if(is_array($patients)): ?>
         <?php $__currentLoopData = $patients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $patient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <?php if(is_array($patient)): ?>
         <?php $__currentLoopData = $patient; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <div class="col-md-6">
            <div class="media mediapro">
                <div class="media-left">
                    <img src="<?php echo e(url('images/noimage.jpg')); ?>" class="media-object" > 
                </div>
                <div class="media-body ">
                    <h4 class="media-heading"><?php echo e($p['firstname']); ?> <?php echo e($p['lastname']); ?> </h4>
                    <a href="<?php echo e(url('deletepatient')); ?>/<?php echo e($p['patientid']); ?>" onClick="return confirm('Are you sure you want to delete this item?');" class="icon-btn hidden">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true hidden"></span>
                    </a>
                    <a href="editpatientpage/<?php echo e($p['patientid']); ?>" class="icon-btn hidden"> 
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

                    <a href="patientfiles/<?php echo e($p['patientid']); ?>" class="icon-btn hidden"> 
                         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    </a>
                    <a href="patientUpdate/<?php echo e($p['patientid']); ?>" class="icon-btn"> 
                        <!--<span class="glyphicon glyphicon-tint" aria-hidden="true"></span>-->
                        <img src="<?php echo e(asset('images/Icon_ondemand-video.png')); ?>" />
                   </a>
                </div>
            </div>
            <hr>
         </div>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         <?php endif; ?>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         <?php endif; ?>
      </div>
   </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>