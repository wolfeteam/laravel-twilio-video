
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<?php echo $__env->yieldContent('title'); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/css/bootstrap.min.css')); ?>">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/css/style2.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('css/font-awesome.min.css')); ?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

     <script type="text/javascript">
        setTimeout(fade_out, 1000);
            function fade_out() {
              $("#alertmsg").fadeOut().empty();
            }
    </script>
 <?php echo $__env->yieldContent('header-script'); ?>
</head>
<body>

<nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                     <a>
                         <img class="logo" src="<?php echo e(url('images/logo.png')); ?>" alt="HHM Logo">
                      </a>
            </div>

             <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-left">
                    
                        <?php echo $__env->yieldContent('menu'); ?> 
                   
                </ul>

                 <ul class="nav navbar-nav navbar-right">
                        <?php if(Auth::user()): ?>
                        <li class="left-bar">
                            <img src="<?php echo e(Auth::user()->image); ?>" class="img-responisve img-circle profile-nav" alt="">   
                        </li>
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo e(Auth::user()->username); ?> <span class="caret"></span>
                            <div><?php echo e(Auth::user()->hospital); ?></div>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                            <li><a href="account.php">Account Settings </a></li>
                                <li><a href="<?php echo e(route('logout')); ?>">Logout</a></li>
                            </ul>
                        </li>
                        <?php else: ?> 
                            <li class="left-bar"><a href="login">Login</a></li>
                            <li class="left-bar"><a href="login">Register</a></li>
                        <?php endif; ?>


                   
                </ul>
            </div>
        </div>
    </nav>

<br><br>


<?php echo $__env->yieldContent('content'); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>    
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>  


</body>
</html>





