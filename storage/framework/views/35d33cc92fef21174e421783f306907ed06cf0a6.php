
<?php $__env->startSection('title'); ?>
<title>Telemed | Call Patient</title>
<?php $__env->stopSection(); ?>
<head>
   <meta name="msapplication-tap-highlight" content="no">
   <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
   <meta name="description" content="Audio+Video+Screen Sharing using RTCMultiConnection" />
   <meta name="keywords" content="WebRTC,RTCMultiConnection,Demos,Experiments,Samples,Examples" />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <style>
      .center{
          text-align:center;
      }
   </style>
</head> 
<?php $__env->startSection('content'); ?> 
<div class="container-fluid">
  <div class="col-md-6 col-md-offset-3 center">
    <h4>Join a Room</h4>
    <p>Enter your name and the name of a room you'd like to join</p>
    <label>your name: </label><input class="btn btn-default " id="identity">
    <label>room name: </label><input class="btn btn-default " id="roomNameS">
    <button class="btn btn-primary" id="joinVideoRoom">Continue</button>
  </div>
</div> 
<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
 
<script type="text/javascript" src="https://code.jquery.com/jquery-1.4.3.min.js" ></script>
<script>
     
    function joinVideoRoom() {
        var roomName = $('#roomNameS').val()
       var identity = $('#identity').val()
       $.ajax({
         type:'POST',
         url:"<?php echo e(route('chat.joinVideoRoom')); ?>",
         data:{'roomName': roomName, 'identity': identity, '_token': $('input[name=_token]').val()},
         success:function(data){
          $("#submitModal").modal('hide')
           if(data.status) {
               window.location.href='fullroom/join/' + data.shortRoom + '/' + data.identity;
           }else
                alert(data.error_msg);
           // completeStatus = 1;
         }
       });
    }
  $('#joinVideoRoom').click(function() {
      joinVideoRoom();
  })
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.public', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>