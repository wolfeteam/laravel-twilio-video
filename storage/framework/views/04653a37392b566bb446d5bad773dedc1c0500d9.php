<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Telemed | Login</title>

  <link rel="stylesheet" type="text/css" href="<?php echo e(url('css/bootstrap.min.css')); ?>">

  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="<?php echo e(url('css/login.css')); ?>">

<script>
        function local(){
        // Check browser support
        if (typeof(Storage) !== "undefined") {

          if(localStorage.profilepic){
            document.getElementById("profilepic").src = localStorage.getItem("profilepic");
            document.getElementById("username").innerHTML = localStorage.getItem("username");
            document.getElementById("username2").innerHTML = localStorage.getItem("username");
            document.getElementById("hospital").innerHTML = localStorage.getItem("hospital");
          }


        } else {
            //document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
        }

      }
</script>

</head>

<body onload="local()">

<div class="container-fluid">

  <div class="row">

    <div class="col-md-12">     


    </div>

  </div>

  

  <br><br>
  <br><br>
  <br><br>
  <div class="row">
    <img src="<?php echo e(url('/images/logo.png')); ?>" alt="HHM TELEMEDICINE" class="center">
  </div>
  <br><br>
  <br><br>
  <br><br>
  <br><br>

  <div class="row">

    <div class="col-md-offset-4 col-md-4">
                    <div class="form lft-part">


        <form class="form-signin" method="post"  action="<?php echo e(url('login')); ?>">
        <?php echo e(csrf_field()); ?>



           <div class="form-group">

       
        <input type="text" class="form-control" placeholder="User Name" id="username" name="username" required />
         <div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
           <?php if($errors->has('email')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('email')); ?></strong>
          </span>
         <?php endif; ?>
         </div>

        <span id="check-e"></span>

        </div>

        

        <div class="form-group">
        
        <input type="password" class="form-control" placeholder="Password" id="password" name="password" required />

        <div class="form-group <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
           <?php if($errors->has('password')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('password')); ?></strong>
          </span>
         <?php endif; ?>
         </div>

        </div>

        <div class="form-group">

            <button type="submit" class="btn btn-primary pull-right" name="btn-login" id="btn-login">

        Log In

      </button> 

            
            <!--<a href="<?php echo e(route('register')); ?>" class="btn btn-default pull-left regbtn"><img src="<?php echo e(asset('images/Icon_ondemand-video.png')); ?>" /></a>-->
          
        </div>  

      </form>

      </div>

    </div>

  </div>

</div>






  <script src="<?php echo e(url('js/jquery.min.js')); ?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?php echo e(url('js/bootstrap.min.js')); ?>"></script>

</body>

</html>

