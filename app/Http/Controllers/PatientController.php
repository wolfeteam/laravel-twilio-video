<?php 
namespace App\Http\Controllers;
use Auth;
use DB;
use Illuminate\Http\Request;
use File;
use App;
use App\AthenaHealth_api\APIConnection;

class PatientController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	 private $api;
	public function __construct()
	{
		$this->middleware('auth');
		$key = '3gk7w5wqaxvfw54vj5mzey9a';
		$secret = 'mjfG68JUf2G8YaT';
		$version = 'v1';
// 		$version = 'preview1';
		$practiceid = 19978;
// 		$practiceid = 1128700;
		$this->api = new APIConnection($version, $key, $secret, $practiceid);
		
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    	

    	 public function getUserPatients()
	    {
	        $Userid=Auth::user()->id;
	        $patients=[];
		  //	echo "<pre>";
		 	// $patientApi= $this->api->GET('/appointmenttypes/');
		 	// 	print_r($patientApi);
		 	// 	exit;
    //           foreach($patientApi['appointmenttypes'] as $val){
            //   $patientApi1= $this->api->GET('/appointments/open',array("departmentid"=>$val['appointmenttypeid']));	
            //   $patientApi1= $this->api->GET('/appointments/open',array("departmentid"=>"1"));	
    //           print_r($patientApi1);
		 	//  }
		 	// exit;	 
	    //     // $pageData=array('search'=>'0','list'=>$patients);
	       // $patientApi= $this->api->GET('/patients/',
	       //     array(
	       //        "guarantorfirstname" => "",
	       //        "dob" => "",
	       //        "firstname" => "aaaa",
	       //        "workphone" => "",
	       //        "departmentid" => "",
	       //        "guarantorsuffix" => "",
	       //        "guarantorlastname" => "",
	       //        "mobilephone" => "",
	       //        "middlename" => "",
	       //        "suffix" => "",
	       //        "guarantormiddlename" => "",
	       //        "lastname" => "",
	       //        "homephone" => "",
	       //        "anyphone" => "",
	       //        "limit" => 100,
	       //        "offset" => 0,
	       //     )
	       // );
		    return view('patient')->with('patients',$patients);
	    }

	    public  static function getUserBackupfiles(){
	    	$Userid=Auth::user()->id;
	        $backupfiles=DB::table('backupfiles')->get();
	        return $backupfiles;
	    }
        public function addappointment(Request $request) {
            $patientApi= $this->api->GET('/appointmenttypes/');
            $pageData = $patientApi["appointmenttypes"];
            $lastItem = $pageData[count($pageData) - 1];
            $appointmentId = $lastItem['appointmenttypeid']+1;
            
            if(
                $appointmentId> 1) {
                $params['appointmentid'] = $appointmentId;
                $params['patientid'] = 12;
                $params['appointmenttypeid'] = 21;
                $params['ignoreschedulablepermission'] = false;
                
                $book_appoint = $this->api->PUT('/appointments/1173/', $params);
                // dd($book_appoint);
            }
            return back();
        }
	   public function searchpatient(Request $request)
		{
			
		    //DB::raw('LOWER(First)')
		    $searchtxt=$request->search;
			//   $patientApiNew=array("search"=>"1");
			//  	if($searchtxt==''){
			// 	$patientApiNew['list']=array();
			// 	}elseif(is_numeric($searchtxt)){
			// 	 $patientApi= $this->api->GET('/patients/',['mobilephone'=>$searchtxt,'limit'=>50]);
			//      $patientApiNew['list']=$patientApi['patients'];
			// 	}else{
			$patient= $this->api->GET('/patients/',['lastname'=>$searchtxt]);
			// if(array_key_exists("notes",$patient[0])){
			// 	$patient[0]['notes']=unserialize($patient[0]['notes']);
			// 	$patient[0]['telemedicine_consent']=$patient[0]['notes']->telemedicine_consent;
			// 	$patient[0]['telemedicine_type']=$patient[0]['notes']->telemedicine_type;
			// }
			//    	print_r($patientApi);
			//    $patientApiNew['list']=$patientApi['patients'];
			// 	}
			// 	$patientApiNew['searchtxt']=$searchtxt;
				// echo $patientApiNew['list'];
            // 	dd($patient);
		    $searchtxt = str_replace(' ', '', $searchtxt);
		    $patients=DB::table('patientprofile')->get(); 
				print_r($patient);
		    return view('patient')->with('patients',$patient);  
		}

		public function patientUpdate($patientid)
		{
			// $patientfiles=DB::table('patientfiles')->where('patientid',$patientid)->get();
			// $patient=DB::table('patientprofile')->where('PatientProfileId', '=', $patientid)->first();
			$patient= $this->api->GET('/patients/'.$patientid);
			
			$appointmenttypes= $this->api->GET('/appointmenttypes/'.$patientid);
			// if(array_key_exists("notes",$patient[0])){
			// 	$patient[0]['notes']=unserialize($patient[0]['notes']);
			// 	$patient[0]['telemedicine_consent']=$patient[0]['notes']->telemedicine_consent;
			// 	$patient[0]['telemedicine_type']=$patient[0]['notes']->telemedicine_type;
			// }
			/*  echo "<pre>";
				print_r( $patientApi[0]);
				exit; */
			//  print_r( $patientApi[0]);

			//   return View("patientfiles")->with(array('patient'=>$patientApi[0]));
			// $approval = DB::table('approvalofpatient')->where('PatientProfileId', '=', $patientid)->orderBy('createAt', 'desc')->take(1)->get();
			return View("patientUpdate", ['patient'=>$patient, 'appointmenttypes'=>$appointmenttypes]);
		}
		
		
		public function patientfiles($patientid)
		    {

		        // $patientfiles=DB::table('patientfiles')->where('patientid',$patientid)->get();
		        $patient=DB::table('patientprofile')->where('PatientProfileId', '=', $patientid)->first();
					 $patientApi= $this->api->GET('/patients/'.$patientid);

					/*  echo "<pre>";
					 print_r( $patientApi[0]);
					 exit; */
					//  print_r( $patientApi[0]);

					//   return View("patientfiles")->with(array('patient'=>$patientApi[0]));
					return View("patientfiles", ['patient'=>$patient]);
		    }

	   public function uppatientfiles(Request $request)
		    {
		         $Filelink  =$request->input('file');
		         $Filename=$request->input('file');
		         $FileType=$request->input('Type');
		         $patientid=$request->input('patientid');

		         if ($request->hasFile('file')){
		            $dest = "files/";
		            $File = str_random(6).'_'.$request->file('file')->getClientOriginalName();
		            $request->file('file')->move($dest,$File);

		            $Filelink='files/'.$File;
		            $Filename= $request->file('file')->getClientOriginalName();
		            }

		       DB::insert('insert into patientfiles (Filelink,Filename,Type,patientid) values(?,?,?,?)',[$Filelink,$Filename,$FileType,$patientid]);

      		return back()->with('status', 'Patient Data has been updated Successfully !');

			 }
			 
			 



   			public function addpatient(Request $request)
			    {
			        $Prefix=$request->input('Prefix');
			        $First=$request->input('First');
			        $Middle=$request->input('Middle');
			        $Last=$request->input('Last');
			        $Suffix=$request->input('Suffix');
			        $Nickname=$request->input('Nickname');
			        $sex=$request->input('sex');
			        $Address1=$request->input('Address1');
			        $Address2=$request->input('Address2');
			        $City=$request->input('City');
			        $State=$request->input('State');
			        $Zip=$request->input('Zip');
			        $Country=$request->input('Country');
			        $County=$request->input('County');
			        $AddressType=$request->input('AddressType');
			        $Phone1=$request->input('Phone1');
			        $Phone1Type=$request->input('Phone1Type');
			        $PhoneCarrier=$request->input('PhoneCarrier');
			        $Phone2=$request->input('Phone2');
			        $Phone2Type=$request->input('Phone2Type');
			        $Phone3=$request->input('Phone3');
			        $Phone3Type=$request->input('Phone3Type');
			        $EMailAddress=$request->input('EMailAddress');
			        $AlternateAddress1=$request->input('AlternateAddress1');
			        $AlternateAddress2=$request->input('AlternateAddress2');
			        $AlternateCity=$request->input('AlternateCity');
			        $AlternateState=$request->input('AlternateState');
			        $AlternateZip=$request->input('AlternateZip');
			        $AlternateCounty=$request->input('AlternateCounty');
			        $AlternateCountry=$request->input('AlternateCountry');
			        $AlternateAddressType=$request->input('AlternateAddressType');
			        $SchoolName=$request->input('SchoolName');
			        $SSN=$request->input('SSN');
			        $Birthdate=$request->input('Birthdate');
			        $DeathDate=$request->input('DeathDate');
			        $ReferredByPatientId=$request->input('ReferredByPatientId');
			        $PatientSameAsGuarantor=$request->input('PatientSameAsGuarantor');
			        $MedicalRecordNumber=$request->input('MedicalRecordNumber');
			        $visdocnum=$request->input('visdocnum');
			        $SSDID=$request->input('SSDID');
			        $ProfileNotes=$request->input('ProfileNotes');
			        $AlertNotes=$request->input('AlertNotes');
			        $AppointmentNotes=$request->input('AppointmentNotes');
			        $BillingNotes=$request->input('BillingNotes');
			        $RegNote=$request->input('RegNote');
			        
			        $userid=Auth::user()->id;
			        

			        /** Check if Photo attached and upload it into Folder"photo" and Save his link into DB*/
			        if ($request->hasFile('photo')){
			            $dest = "photo/";
			            $Photoname = str_random(6).'_'.$request->file('photo')->getClientOriginalName();

			            $request->file('photo')->move($dest,$Photoname);

			            $Photo='images/patientspic/'.$Photoname;

			        } 
			        else {
			        	$Photo='images/noimage.jpg';
			        }

					$patients= $this->api->POST('/patients/',[
					'Prefix' => $Prefix , 
					'First' => $First , 
					'Middle' => $Middle , 
					'Last' => $Last , 
					'Suffix' => $Suffix , 
					'Nickname' => $Nickname , 
					'sex' => $sex , 
					'Address1' => $Address1 , 
					'Address2' => $Address2 , 
					'City' => $City , 
					'State' => $State , 
					'Zip' => $Zip , 
					'Country' => $Country , 
					'County' => $County , 
					'AddressType' => $AddressType , 
					'Phone1' => $Phone1 , 
					'Phone1Type' => $Phone1Type , 
					'PhoneCarrier' => $PhoneCarrier , 
					'Phone2' => $Phone2 , 
					'Phone2Type' => $Phone2Type , 
					'Phone3' => $Phone3 , 
					'Phone3Type' => $Phone3Type , 
					'EMailAddress' => $EMailAddress , 
					'AlternateAddress1' => $AlternateAddress1 , 
					'AlternateAddress2' => $AlternateAddress2 , 
					'AlternateCity' => $AlternateCity , 
					'AlternateState' => $AlternateState , 
					'AlternateZip' => $AlternateZip , 
					'AlternateCounty' => $AlternateCounty , 
					'AlternateCountry' => $AlternateCountry , 
					'AlternateAddressType' => $AlternateAddressType , 
					'SchoolName' => $SchoolName , 
					'SSN' => $SSN , 
					'Birthdate' => $Birthdate , 
					'DeathDate' => $DeathDate , 
					'ReferredByPatientId' => $ReferredByPatientId , 
					'PatientSameAsGuarantor' => $PatientSameAsGuarantor , 
					'MedicalRecordNumber' => $MedicalRecordNumber , 
					'visdocnum' => $visdocnum , 
					'SSDID' => $SSDID , 
					'ProfileNotes' => $ProfileNotes , 
					'AlertNotes' => $AlertNotes , 
					'AppointmentNotes' => $AppointmentNotes , 
					'BillingNotes' => $BillingNotes , 
					'RegNote' => $RegNote , 
					'Picture' => $Photo , ]);
					
					
			        // DB::table('patientprofile')->insert([
			                
			        //         'Prefix' => $Prefix , 
			        //         'First' => $First , 
			        //         'Middle' => $Middle , 
			        //         'Last' => $Last , 
			        //         'Suffix' => $Suffix , 
			        //         'Nickname' => $Nickname , 
			        //         'sex' => $sex , 
			        //         'Address1' => $Address1 , 
			        //         'Address2' => $Address2 , 
			        //         'City' => $City , 
			        //         'State' => $State , 
			        //         'Zip' => $Zip , 
			        //         'Country' => $Country , 
			        //         'County' => $County , 
			        //         'AddressType' => $AddressType , 
			        //         'Phone1' => $Phone1 , 
			        //         'Phone1Type' => $Phone1Type , 
			        //         'PhoneCarrier' => $PhoneCarrier , 
			        //         'Phone2' => $Phone2 , 
			        //         'Phone2Type' => $Phone2Type , 
			        //         'Phone3' => $Phone3 , 
			        //         'Phone3Type' => $Phone3Type , 
			        //         'EMailAddress' => $EMailAddress , 
			        //         'AlternateAddress1' => $AlternateAddress1 , 
			        //         'AlternateAddress2' => $AlternateAddress2 , 
			        //         'AlternateCity' => $AlternateCity , 
			        //         'AlternateState' => $AlternateState , 
			        //         'AlternateZip' => $AlternateZip , 
			        //         'AlternateCounty' => $AlternateCounty , 
			        //         'AlternateCountry' => $AlternateCountry , 
			        //         'AlternateAddressType' => $AlternateAddressType , 
			        //         'SchoolName' => $SchoolName , 
			        //         'SSN' => $SSN , 
			        //         'Birthdate' => $Birthdate , 
			        //         'DeathDate' => $DeathDate , 
			        //         'ReferredByPatientId' => $ReferredByPatientId , 
			        //         'PatientSameAsGuarantor' => $PatientSameAsGuarantor , 
			        //         'MedicalRecordNumber' => $MedicalRecordNumber , 
			        //         'visdocnum' => $visdocnum , 
			        //         'SSDID' => $SSDID , 
			        //         'ProfileNotes' => $ProfileNotes , 
			        //         'AlertNotes' => $AlertNotes , 
			        //         'AppointmentNotes' => $AppointmentNotes , 
			        //         'BillingNotes' => $BillingNotes , 
			        //         'RegNote' => $RegNote , 
			        //         'Picture' => $Photo , 
			        //      ]); 

			        return redirect('viewpatient')->with('status','The Patient Data has been added Successfully');;

			    }


		 
			
			public function editpatientpage($patientid)
		    {

		        // $patient=DB::table('patientprofile')->where([
		        //     ['PatientProfileId','=',$patientid]
		        //     ])->first(); 
					
				$patient= $this->api->GET('/patients/'.$patientid)[0];
				if(array_key_exists("notes",$patient)){
					$patient['notes']=unserialize($patient['notes']);
					$patient['telemedicine_consent']=$patient['notes']->telemedicine_consent;
					$patient['telemedicine_type']=$patient['notes']->telemedicine_type;
				}
				  // return view('editpatient',[compact('patient')]);
              	return view('editpatient',['patient'=>$patient]);
				  
		    }
    

		    public function editpatient(Request $request)
		    {
				$patient= $this->api->PUT('/patients/'.$request->input('patientid'),['email'=>$request->input('email'),'mobilephone'=>$request->input('mobilephone'),'mobilecarrierid'=>$request->input('mobilecarrierid')]);
				
				return redirect('patientUpdate/'.$request->input('patientid'));

		    }



		    public function deletepatient($patientid)
			    {

			         /*secure  and delete Code */
			         if (DB::table('patientprofile')->where([
			            ['PatientProfileId','=',$patientid]
			            ])->delete() == true) {

			         $patients=DB::table('patientprofile')->paginate('50');
			         $data = [
						    'patients'  => $patients,
						    'status'   => 'The Patient Data has been Deleted Successfully',
						];

			        	return redirect('viewpatient')->with($data);
			    	}

			        else {
			        	$patients=DB::table('patientprofile')->paginate('50');
			           return redirect('viewpatient')->with('patients',$patients);
			        }    

			    }



			    public function DeleteBackup($backupid)
			    {	

			    	//get file name 
			         	$filename = DB::table('backupfiles')->where([
			            ['id','=',$backupid]
			            ])->value('link');

			         /*secure  and delete Code */
			         if (DB::table('backupfiles')->where([
			            ['id','=',$backupid]
			            ])->delete() == true) {

			         	
			         	$filepath = 'backupfiles/'.$filename.'.sql';
			         	File::delete($filepath);
			        return back()->with('status','The Patient Backupfile has been Deleted Successfully');
			        }

			        else {
			           return back(); 
			        }    

			    }


			     public function deletefile($patientfileid)
				    {
				        /*secure Code */ /*Check if the file is belong to patient which that belong to current user or not*/
				        $patientfile=DB::table('patientfiles')->where('id',$patientfileid)->first();
				        $patientid=$patientfile->patientid;

				        if (DB::table('patientprofile')->where([
				            ['PatientProfileId','=',$patientid]
				            ])->first() == true)

				        {
				        /*Dlete code*/
				         DB::table('patientfiles')->where('id',$patientfileid)->delete();
				        return back()->with('status','The Patient File Data has been Deleted Successfully');
				         }

				         else {
				            return back();
				         }

				    }




				public function ajaxgetpatient(Request $request)
			    {
			    
			        if ($request->ajax()){

			            $patientid = $request->input('patientid');
			            $patientfiles=DB::table('patientfiles')->where('patientid',$patientid)->get();
			              
			             foreach ($patientfiles as $patientfile ){        
			                     echo ' 
			                      <option value='.$patientfile->Filelink.'>'.$patientfile->Filename.'</option>
			                     ';
			                 }  

			            }


				}

				public function ajaxUpdateSetupPatient(Request $request){
					$patient= $this->api->GET('/patients/'.$request->input('patientid'));
					$providers= $this->api->GET('/providers');
					$departments= $this->api->GET('/departments');
					$data = new \stdClass();
					$data->patient = $patient;
					$data->providers = $providers;
					$data->departments = $departments;
					return json_encode($data);
				}

				public function ajaxUpdateGetApproval(Request $request){
					if ($request->ajax()){
					    $patientid = $request->input('patientid');
						$answer1 = $request->input('answer1');
						$answer2 = $request->input('answer2');
						$notes="Telemedicine Consent = ".$answer1." ".date('Y-m-d H:i:s a'). "\r\n" ."Telemedicine Type = ".$answer2 ;
						$patient= $this->api->PUT('/patients/'.$request->input('patientid'),['notes'=>$notes]);
						
						$params['departmentid'] = $request->input('fdepartments');
						$params['providerid']   = $request->input('fproviders');
						
					    $patientappointmentreasons = $this->api->GET('/patientappointmentreasons', $params);
					    
					    if($patientappointmentreasons['totalcount'] == 0) {
					       // $temp = 
					    }
						$appointmentTypes= $this->api->GET('/appointmenttypes');
						$data = new \stdClass();
						$data->answer1 = $answer1;
						$data->providers  = $request->input('fproviders');
						$data->departments= $request->input('fdepartments');
						$data->patient=$patient;
						$data->patientappointmentreasons=$patientappointmentreasons;
						$data->appointmentTypes=$appointmentTypes;
						return json_encode($data);
			        }
				}

				public function ajaxUpdateCreateAppointment(Request $request){
					if ($request->ajax()){
					    $patientid = $request->input('patientid');
					    $datetemp = explode(' ', $request->input('datetime'));
					    $date = $datetemp[0];
					    $time = $datetemp[1];
					    
						$providerid = $request->input('providerid');
						$bookingnote = $request->input('note');
						 
						$appointmentReason = $request->input('patientappointmentreasons');
						 
						$params['patientid'] = $patientid;
						$params['providerid'] = $providerid;
                        $params['departmentid'] = $request->input('departmentid');
                        $params['appointmenttypeid'] = $request->input('appointmenttypeid');
                        $params['bookingnote'] = $bookingnote;
                        $params['appointmentdate'] = $date;
                        $params['appointmenttime'] = $time;
                        
                        $params['ignoreschedulablepermission'] = true;
                        $params['reasonid'] = -1;
                        
						$appointmentOpen = $this->api->GET('/appointments/open', $params);
						
						$appointmentId = 0;
						if($appointmentOpen['totalcount'] == 0) {
						    $params['reasonid'] = $appointmentReason;
						    $appointmentRe = $this->api->POST('/appointments/open', $params);
						    $appointmentId = array_keys($appointmentRe['appointmentids'])[0];
						}else {
						    $appointmentId = $appointmentOpen['appointments'][0]['appointmentid'];
						}
						
						$book_appoint = array();
                        if($appointmentId> 1) {
                            $params['appointmentid'] = $appointmentId;
                            $params['reasonid'] = $appointmentReason;
                            $book_appoint = $this->api->PUT('/appointments/'.$appointmentId.'/', $params);
                        }
                        // return back();
                        
						return json_encode($book_appoint);
			        }
				}
				
				public function ajaxUpdateHaveMeeting(Request $request){
					if ($request->ajax()){
						$patientid = $request->input('patientid');
						DB::table('patientprofile')->where('PatientProfileId',$patientid)->update(["completeStatus"=>4]);
						echo 1;
			        }
				}


				public function ajaxgetcallid(Request $request)
        		{
         			$patientid = $request->input('patientid');
            		$patient=DB::table('patientprofile')->where('PatientProfileId',$patientid)->first();

                     echo ' 
                      <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
							<div style="text-align:center;padding:15px;">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" style="color:#337ab7">Send patient telemedicine link</h4>
								<h5 class="modal-title" style="color:#337ab7">Click inside text to paste room url</h5>
							</div>
							<div class="col-md-3 text-right" style="border-right:2px solid #337ab7;padding-right: 0px;">
            					<div class="modal-body">
									<div class="row">
										<p style="padding-right: 12px;color:#337ab7">Select a room</p>
										<div>
											<p style="background-color:#337ab7;padding-right: 20px;color:white">Room1</p>
											<p style="padding-right: 20px;color:#337ab7">Room2</p>
											<p style="padding-right: 20px;color:#337ab7">Room3</p>
											<p style="padding-right: 20px;color:#337ab7">Room4</p>
											<p style="padding-right: 20px;color:#337ab7">Room5</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-9 ml-auto">
								<div class="modal-body">
								  	<div class="row">
									  	<form target="_blank" method="post" action="sendemail">
											<div class="form-group">
												<p style="color:#337ab7">Send link via email</p>
												<input name="message" id="room-id1" onclick="return getlink();" value="" class="form-control">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="name" value="Patient System">
												<input type="hidden" name="mail" value="{{$patient->EMailAddress}}">
												<input type="hidden" name="subject" value="Chat-Request">
											</div>
											<button style="float:right" type="submit" class="btn btn-primary btn-cus">Send Email</button>
										</form>
							  		</div>
									<div class="row">
									  <form target="_blank" method="post" action="sendsms">
										  <div class="form-group">
											  <p style="color:#337ab7">Send link via sms</p>
											  <input name="message" id="room-id2" onclick="return getlink();" value="" class="form-control">
											  <input type="hidden" name="_token" value="{{ csrf_token() }}">
											  <input type="hidden" name="name" value="Patient System">
											  <input type="hidden" name="mobile" value="{{$patient->Phone1}}">
											  <input type="hidden" name="PhoneCarrier" value="{{$patient->PhoneCarrier}}">
											  <input type="hidden" name="subject" value="Chat-Request">
										  </div>
										  <button style="float:right" type="submit" class="btn btn-primary btn-cus">Send SMS</button>
									  </form>
									</div>
								</div>
        					</div>
							<div class="modal-footer">
							</div>
                        c</div>
                        </div>
                     ';      
         

    			}



}
