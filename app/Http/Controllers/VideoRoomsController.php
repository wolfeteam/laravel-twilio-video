<?php 
namespace App\Http\Controllers;
use Auth;
use DB;
use Illuminate\Http\Request;
use File;
use App;
use App\AthenaHealth_api\APIConnection;
use IlluminateHttpRequest;
use TwilioRestClient;
use TwilioJwtAccessToken;
use TwilioJwtGrantsVideoGrant;
   
class VideoRoomsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	protected $sid;
    protected $token;
    protected $key;
    protected $secret;
	private $api;
	public function __construct()
	{
	    $this->sid = config('services.twilio.sid');
        $this->token = config('services.twilio.token');
        $this->key = config('services.twilio.key');
        $this->secret = config('services.twilio.secret');
		$this->middleware('auth');
		$key = '3gk7w5wqaxvfw54vj5mzey9a';
		$secret = 'mjfG68JUf2G8YaT';
		$version = 'v1';
		$practiceid = 195900;
		/* practiceid = 1128700; */
		$this->api = new APIConnection($version, $key, $secret, $practiceid);
	}
	
	public function index()
    {
       $rooms = [];
       try {
           $client = new Client($this->sid, $this->token);
           $allRooms = $client->video->rooms->read([]);
    
            $rooms = array_map(function($room) {
               return $room->uniqueName;
            }, $allRooms);
    
       } catch (Exception $e) {
           echo "Error: " . $e->getMessage();
       }
       return view('index', ['rooms' => $rooms]);
    }
    
    public function createRoom(Request $request)
    {
       $client = new Client($this->sid, $this->token);
    
       $exists = $client->video->rooms->read([ 'uniqueName' => $request->roomName]);
    
       if (empty($exists)) {
           $client->video->rooms->create([
               'uniqueName' => $request->roomName,
               'type' => 'group',
               'recordParticipantsOnConnect' => false
           ]);
    
           \Log::debug("created new room: ".$request->roomName);
       }
    
      return redirect()->action('ChatController@joinRoom', [
          'roomName' => $request->roomName
      ]);
    }
    public function joinRoom($roomName)
    {
       // A unique identifier for this user
       $identity = Auth::user()->name;
    
       \Log::debug("joined with identity: $identity");
       $token = new AccessToken($this->sid, $this->key, $this->secret, 3600, $identity);
    
       $videoGrant = new VideoGrant();
       $videoGrant->setRoom($roomName);
    
       $token->addGrant($videoGrant);
    
       return view('room', [ 'accessToken' => $token->toJWT(), 'roomName' => $roomName ]);
    }
}
