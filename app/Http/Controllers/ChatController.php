<?php 
namespace App\Http\Controllers;
use Auth;
use DB;
use Illuminate\Http\Request;
use File;
use App;
use App\AthenaHealth_api\APIConnection;
use IlluminateHttpRequest;
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

class ChatController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	protected $sid;
    protected $token;
    protected $key;
    protected $secret;
	private $api;
	
	public function __construct()
	{
	    $this->sid = config('services.twilio.sid');
        $this->token = config('services.twilio.token');
        $this->key = config('services.twilio.key');
        $this->secret = config('services.twilio.secret');
        
	//	$this->middleware('auth');
		$key = '3gk7w5wqaxvfw54vj5mzey9a';
		$secret = 'mjfG68JUf2G8YaT';
		$version = 'v1';
		$practiceid = 195900;
		/* practiceid = 1128700; */
		$this->api = new APIConnection($version, $key, $secret, $practiceid);
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    public function generate_token($room_name,$name="")
    {
        // Substitute your Twilio Account SID and API Key details
        $accountSid = $this->sid;
        $apiKeySid = $this->key;
        $apiKeySecret = $this->secret;

        $identity =$name? $name: uniqid();

        // Create an Access Token
        $token = new AccessToken(
            $accountSid,
            $apiKeySid,
            $apiKeySecret,
            3600,
            $identity
        );

        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom($room_name);
        $token->addGrant($grant);

        // Serialize the token as a JWT
        return $token->toJWT();
    }
    public function appointment(Request $request){
        return view('appointment',[]);
    }
	 public function chat(Request $request)
    {   
        
      // $params['enddate'] =  'enddate';
      
        // $params['startdate'] = startdate;
    // 	$patientApi= $this->api->GET('/appointments/booked/', $params);
    // 	dd($patientApi);
// 		$appointments=DB::table('appointment')->orderBy('createAt', 'desc')->paginate('2');

        // $appointments = $request->all();
        $appointments = array(
          "_token" => "ZfsRzGAM7vli28m2ZZix3U5IUjGFoHcotpc6zN1W",
          "appointmentid" => "1173987",
          "appointmenttypeid" => "4",
          "departmentid" => "1",
          "providerid" => "344",
          "patientid" => "822",
          "appointmenttype" => "PHYSICAL EXAM",
          "appointment_datetime" => "07/31/2020 12:00",
          "patientname" => "Maxime Johnson",
          "patientemail" => "damdffdsodar.p@ha.com",
          "mobilephone" => "8476964365"
         );
        
		return view('call', ['appointment'=>$appointments]);
		
    }
    
    public function createRoom(Request $request) {
        $room          = $request->input('roomName');
        $appointmentId = $request->input('appointmentId');
        $room_url = $error_msg = '';
        $roomName = $room.'-'.$appointmentId.'-'.strtotime(date('Y-m-d hh:ii:ss'));
        $client = new Client($this->sid, $this->token);
        $exists = $client->video->rooms->read([ 'uniqueName' => $roomName]);
        $status = false;
        $token = $this->generate_token($room);
        try {
           $result = $client->video->rooms->create([
               'uniqueName' => $roomName,
               'type' => 'group',
               'recordParticipantsOnConnect' => false
           ]);
        //   $room_url = $result->url;
           $status = true;
        }catch(\Exception $ex) {
            $error_msg = $ex->getMessage();
        }
        $uniqueRoomName = $roomName;
        $roomName = url('/room/join/'.$roomName);
        return ['status'=>$status, 'roomName' => $roomName, 'shortRoom' => $uniqueRoomName, 'error_msg'=>$error_msg];
    }
    
    public function joinVideoRoom(Request $request) {
        $room          = $request->input('roomName');
        $identity = $request->input('identity');
        $room_url = $error_msg = '';
        $roomName = $room.'-'.strtotime(date('Y-m-d hh:ii:ss'));
        $client = new Client($this->sid, $this->token);
        $exists = $client->video->rooms->read([ 'uniqueName' => $roomName]);
        $status = false;
        
        if(empty($exists)){
            $client->video->rooms->create([
                'uniqueName' => $roomName,
                'type' => 'group',
                'recordParticipantsOnConnect' => false
            ]);            
        }
        $status = true;
        $uniqueRoomName = $roomName;
        $roomName = url('/room/join/'.$roomName);
        return ['status'=>$status, 'roomName' => $roomName,'identity'=>$identity ,'shortRoom' => $uniqueRoomName, 'error_msg'=>$error_msg];
    }

    public function fullJoinRoom($roomName,$identity){
        $token = $this->generate_token($roomName,$identity);
    
        $videoGrant = new VideoGrant();
        $videoGrant->setRoom($roomName);
     
        return view('fullRoom', [ 'accessToken' => $token, 'roomName' => $roomName ]);
    }
    public function joinRoom($roomName)
    {
       // A unique identifier for this user
     //  $identity = Auth::user()->name;
    
    //   \Log::debug("joined with identity: $identity");
       $token = $this->generate_token($roomName);
    
       $videoGrant = new VideoGrant();
       $videoGrant->setRoom($roomName);
    
       return view('room', [ 'accessToken' => $token, 'roomName' => $roomName ]);
    }
    public function sendSMS(Request $request) {
        $message  = $request->input('message');
        $to = $request->input('phoneNum');
         
        $twilioNumber = env('TWILIO_NUMBER', 3122626148);
        
        // $to = '312-273-8058';
        $client = new Client($this->sid, $this->token);
        $status = false;
        $result = '';
        try {
            $client->messages->create(
                $to,
                [
                    "body" => $message,
                    "from" => $twilioNumber
                ]
            );
           
            $status = true;
            $result = 'Message sent to ' . $to;
        } catch (TwilioException $e) {
            $result = 'Twilio replied with: ' . $e;
        }
        
        return ['status'=>$status, 'result'=>$result];
    }

}
