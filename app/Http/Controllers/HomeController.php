<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\patient;
use Mail;
use Illuminate\Support\Facades\Hash;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    


    public function index()
    {
        return view('home');
    }


    public function addpatientpage()
    {
        return view('addpatient');
    }

    public function editpatientpage($patientid)
    {

        $patient=DB::table('patients')->where([
            ['id','=',$patientid],
            ['userid','=',Auth::user()->id]
            ])->first(); 

        return view('editpatient',compact('patient'));
    }
    

    public function viewpatient()
    {
       
       
        $patients=DB::table('patients')->where('userid',Auth::user()->id)->get();
        return view('patient')->with('patients',$patients);
    }



    public function editpatient(Request $request)
    {

        $id=$request->input('patientid');
        $name=$request->input('Name');
        $Age=$request->input('Age');
        $Mobile=$request->input('Mobile');
        $Landline=$request->input('Landline');
        $Address=$request->input('Address');
        $email=$request->input('email');
        $sex=$request->input('Sex');
        $note=$request->input('Notes');


        /** Check if Photo attached and upload it into Folder"photo" and Save his link into DB*/
        if ($request->hasFile('photo')){
            $dest = "photo/";
            $Photoname = str_random(6).'_'.$request->file('photo')->getClientOriginalName();

            $request->file('photo')->move($dest,$Photoname);

            $Photo='photo/'.$Photoname;

            DB::update('update patients set Name=?,Age=?,Mobile=?,Landline=?,Address=?,email=?,Sex=?,Notes=?,Photo=? where id=? ',[$name,$Age,$Mobile,$Landline,$Address,$email,$sex,$note,$Photo,$id]);

        } 
  
       else {
        DB::update('update patients set Name=?,Age=?,Mobile=?,Landline=?,Address=?,email=?,Sex=?,Notes=? where id=? ',[$name,$Age,$Mobile,$Landline,$Address,$email,$sex,$note,$id]);
        }

        return back()->with('status', 'Patient Data has been updated Successfully !');

    }


     public function deletepatient($patientid)
    {

         /*secure  and delete Code */
         if (DB::table('patients')->where([
            ['id','=',$patientid],
            ['userid','=',Auth::user()->id]
            ])->delete() == true) {

         DB::table('patientfiles')->where('patientid',$patientid)->delete();
        return back()->with('status','The Patient Data has been Deleted Successfully');
        }

        else {
           return back(); 
        }    

    }


     public function deletefile($patientfileid)
    {
        /*secure Code */ /*Check if the file is belong to patient which that belong to current user or not*/
        $patientfile=DB::table('patientfiles')->where('id',$patientfileid)->first();
        $patientid=$patientfile->patientid;

        if (DB::table('patients')->where([
            ['id','=',$patientid],
            ['userid','=',Auth::user()->id]
            ])->first() == true)

        {
        /*Dlete code*/
         DB::table('patientfiles')->where('id',$patientfileid)->delete();
        return back()->with('status','The Patient File Data has been Deleted Successfully');
         }

         else {
            return back();
         }

    }



    public function addpatient(Request $request)
    {
        
        $name=$request->input('name');
        $Age=$request->input('Age');
        $Mobile=$request->input('Mobile');
        $Landline=$request->input('Landline');
        $Address=$request->input('Address');
        $email=$request->input('email');
        $sex=$request->input('sex');
        $note=$request->input('note');
        $Files=$request->input('file');
        $Photo=$request->input('photo');
        $userid=Auth::user()->id;
        

        /** Check if Photo attached and upload it into Folder"photo" and Save his link into DB*/
        if ($request->hasFile('photo')){
            $dest = "photo/";
            $Photoname = str_random(6).'_'.$request->file('photo')->getClientOriginalName();

            $request->file('photo')->move($dest,$Photoname);

            $Photo='photo/'.$Photoname;

        } 

         DB::insert('insert into patients (userid,Name,Age,Mobile,Landline,Address,email,Sex,Notes,Files,Photo) values(?,?,?,?,?,?,?,?,?,?,?)',[$userid,$name,$Age,$Mobile,$Landline,$Address,$email,$sex,$note,$Files,$Photo]);

        return redirect('viewpatient')->with('status','The Patient Data has been added Successfully');;

    }


    public function callpatient()
    {
       
        $patients=DB::table('patients')->get();
        return view('call')->with('patients',$patients);
    }


    public function sendemail(Request $request)
    {
         
        $email =$request->input('mail');
        $name='HHM';
        $title='HHM Chatroom Request';
        $messagetext =$request->input('message');

        $data = array( 'email' => $email , 'name' => $name , 'from' => 'info@unfcclients.com' , 'from_name' => $name, 'subject' => $title ,'msg' => $messagetext ) ;

        Mail::raw( $data['msg'], function($message) use ($data) {
        $message->to($data['email'], 'Patient System')->subject($data['subject']);
        $message->from( $data['from'] ,$data['from_name']);

      });

        return back()->with('status', 'Message has been Sent Successfully');
      
      
    }



    public function sendsms(Request $request)
    {
         
        dd("ok");
        $mobile=$request->input('mobile');
        $PhoneCarrier=$request->input('PhoneCarrier');
        $email = $mobile.'@'.$PhoneCarrier;
        $name='HHM';
        $title='HHM Chatroom Request';
        $messagetext =$request->input('message');

        $data = array( 'email' => $email , 'name' => $name , 'from' => 'info@unfcclients.com' , 'from_name' => $name, 'subject' => $title ,'msg' => $messagetext ) ;

        Mail::raw( $data['msg'], function($message) use ($data) {
        $message->to($data['email'], 'Patient System')->subject($data['subject']);
        $message->from( $data['from'] ,$data['from_name']);

      });

        return back()->with('status', 'Message has been Sent Successfully');
      
      
    }


    public function chgpwd (Request $request)
    {

        $oldpwd=$request->input('oldpwd');
        $newpwd=Hash::make($request->input('password'));
        $userid=$request->input('user_id');

        $user=DB::table('users')->where('id',$userid)->first();
                if (Hash::check($oldpwd, $user->password)== true){
                    DB::update('update users set password=? where id=?',[$newpwd,$userid]);
                    $message="Your password has been Changed Successfully ";    
                   
                }
                else {
                    $message="Warning : The Old password is inCorrect , Please Try Again ";

                }
    
        return back()->with('status',$message );
        
    }

    public function DeleteAccount (Request $request)
    {
        $userid=$request->input('user_id');
        $patients=DB::table('patients')->where('userid',$userid)->get(); 
        
        foreach ($patients as $patient ) {
            DB::table('patientfiles')->where('patientid',$patient->id)->delete(); 
            DB::table('patients')->where('id',$patient->id)->delete();
        }

       
        DB::table('users')->where('id',$userid)->delete();

        return redirect('home');
        
    }


    

    

    
    public function setup(){
        return view('setup');
    }

    
}

   

