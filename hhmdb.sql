-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 16, 2021 at 05:34 PM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hhmdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) UNSIGNED NOT NULL,
  `patientProfileId` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `email` varchar(55) DEFAULT NULL,
  `phone` varchar(55) NOT NULL,
  `message` text,
  `datetimeAppointment` datetime DEFAULT NULL,
  `createAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `patientProfileId`, `name`, `email`, `phone`, `message`, `datetimeAppointment`, `createAt`) VALUES
(1, 75, 'Eric E Austin', 'kosong0926@gmail.com', '7738751338', NULL, NULL, '2020-04-22 16:51:43'),
(2, 6898, 'Mari  Hernandez', 'maricelahernandez@hhmsllc.com', '8159198777', 'Testing this now', '2020-04-22 13:30:00', '2020-04-22 18:27:13'),
(3, 6899, 'Dennis  Bender', '1@1.com', '7737919275', NULL, NULL, '2020-04-29 19:10:25'),
(4, 6900, 'Ernest  Ellison', '1@1.com', '7737019640', NULL, NULL, '2020-05-01 20:07:36'),
(5, 6901, 'Ronald  Sam', 'drronaldsam@hhmsllc.com', '7735193648', NULL, NULL, '2020-05-01 20:40:50');

-- --------------------------------------------------------

--
-- Table structure for table `approvalofpatient`
--

CREATE TABLE `approvalofpatient` (
  `id` int(11) UNSIGNED NOT NULL,
  `patientProfileId` int(11) NOT NULL,
  `answer1` varchar(10) DEFAULT NULL,
  `answer2` varchar(20) DEFAULT NULL,
  `createAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `approvalofpatient`
--

INSERT INTO `approvalofpatient` (`id`, `patientProfileId`, `answer1`, `answer2`, `createAt`) VALUES
(1, 75, 'yes', 'appointment', '2020-04-22 16:51:40'),
(2, 6898, 'yes', 'phone', '2020-04-22 18:26:17'),
(3, 6899, 'yes', 'phone', '2020-04-29 19:09:52'),
(4, 6900, 'yes', 'appointment', '2020-05-01 20:07:16'),
(5, 6901, 'yes', 'appointment', '2020-05-01 20:40:48'),
(6, 1, 'yes', 'appointment', '2020-05-02 08:06:11');

-- --------------------------------------------------------

--
-- Table structure for table `backupfiles`
--

CREATE TABLE `backupfiles` (
  `id` int(10) NOT NULL,
  `link` varchar(150) NOT NULL,
  `Createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backupfiles`
--

INSERT INTO `backupfiles` (`id`, `link`, `Createdate`) VALUES
(18, '15870User-Backup', '2018-03-12 16:47:38'),
(20, '10229Admin-Backup', '2018-03-24 04:25:00'),
(21, '488030571Admin-Backup', '2020-03-25 05:20:10'),
(22, '1103312483Admin-Backup', '2020-04-18 11:20:22'),
(23, '1688443800Admin-Backup', '2020-04-18 11:20:27'),
(24, '1112487867Admin-Backup', '2020-04-18 11:20:31'),
(25, '1748836170Admin-Backup', '2020-04-22 16:09:00'),
(26, '394763702Admin-Backup', '2021-02-06 00:03:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2014_10_12_000000_create_users_table', 1),
(11, '2014_10_12_100000_create_password_resets_table', 1),
(12, '2017_06_05_173042_create_patients_table', 1),
(13, '2017_06_16_235754_patientfiles', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patientfiles`
--

CREATE TABLE `patientfiles` (
  `fileId` int(11) NOT NULL,
  `Filelink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Filename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patientid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patientprofile`
--

CREATE TABLE `patientprofile` (
  `PatientProfileId` int(11) NOT NULL,
  `Prefix` varchar(50) DEFAULT NULL,
  `First` varchar(50) DEFAULT NULL,
  `Middle` varchar(50) DEFAULT NULL,
  `Last` varchar(50) DEFAULT NULL,
  `Suffix` varchar(50) DEFAULT NULL,
  `Nickname` varchar(50) DEFAULT NULL,
  `Address1` varchar(100) DEFAULT NULL,
  `Address2` varchar(100) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `State` varchar(50) DEFAULT NULL,
  `Zip` varchar(50) DEFAULT NULL,
  `Country` varchar(50) DEFAULT NULL,
  `AddressType` varchar(50) DEFAULT NULL,
  `Phone1` varchar(50) DEFAULT NULL,
  `Phone1Type` varchar(50) DEFAULT NULL,
  `PhoneCarrier` varchar(55) DEFAULT NULL,
  `Phone2` varchar(50) DEFAULT NULL,
  `Phone2Type` varchar(50) DEFAULT NULL,
  `Phone3` varchar(50) DEFAULT NULL,
  `Phone3Type` varchar(50) DEFAULT NULL,
  `County` varchar(50) DEFAULT NULL,
  `EMailAddress` varchar(100) DEFAULT NULL,
  `AlternateAddress1` varchar(100) DEFAULT NULL,
  `AlternateAddress2` varchar(100) DEFAULT NULL,
  `AlternateCity` varchar(50) DEFAULT NULL,
  `AlternateState` varchar(50) DEFAULT NULL,
  `AlternateZip` varchar(50) DEFAULT NULL,
  `AlternateCounty` varchar(50) DEFAULT NULL,
  `AlternateCountry` varchar(50) DEFAULT NULL,
  `AlternateAddressType` varchar(50) DEFAULT NULL,
  `SchoolName` varchar(50) DEFAULT NULL,
  `SSN` varchar(50) DEFAULT NULL,
  `Birthdate` datetime DEFAULT NULL,
  `DeathDate` datetime DEFAULT NULL,
  `Sex` varchar(50) DEFAULT NULL,
  `ReferredByPatientId` int(11) DEFAULT NULL,
  `PatientSameAsGuarantor` int(11) DEFAULT NULL,
  `MedicalRecordNumber` varchar(50) DEFAULT NULL,
  `ProfileNotes` text,
  `AlertNotes` text,
  `AppointmentNotes` text,
  `BillingNotes` text,
  `Picture` varchar(150) DEFAULT 'images/noimage.jpg',
  `visdocnum` varchar(50) DEFAULT NULL,
  `SSDID` varchar(50) DEFAULT NULL,
  `Created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `RegNote` text,
  `completeStatus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/image_placeholder.png',
  `confirmed` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `title`, `username`, `hospital`, `image`, `confirmed`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'User', 'Dr', 'User', 'USA', 'images/image_placeholder.png', 'confirmed', 'User@example.com', '$2y$10$UMR3nxvNPDHCst3gQDl.NO3dhYp0JE1hiqY5LbFcZcp5l6FX/rPBO', 'AcgmSYVsREHJG1ebGR3WlmYN8vj2sFFqcSub1reuRSG67xzxe9DfxphJk6sG', '2018-03-05 14:12:43', '2018-03-05 14:12:43'),
(27, 'Drsam', 'Mr', 'Drsam', 'USA', 'images/image_placeholder.png', 'confirmed', 'Drsam@Drsam.com', '$2y$10$/BeUOPRSw4u8ur.hqynrxuDdLsMvKL.YiwAcm4n1C3.7qCe.7gs82', 'uTo3wQSFWIR286sOkaQR1mzke21L6uA8V9TfzLJKPtxNP0b2Z0PacjymQ9gz', '2018-03-24 01:07:29', '2018-03-24 01:07:29'),
(28, 'Mari', 'Mr', 'Mari', 'USA', 'images/image_placeholder.png', 'confirmed', 'Mari@Mari.com', '$2y$10$T8aTkTnq5EeNvmkl4xtrZe9slH8Xixe7em1zFnXMuE/NprNRn7X9C', 'aKnRDnqcwFrT2eJYsTLB2X6YjEQSsLaCOsI4D13UlbPB0feI95X6LRqBabXX', '2018-03-24 01:08:11', '2018-03-24 01:08:11'),
(29, 'Admin', 'Mr', 'Admin', 'USA', 'images/image_placeholder.png', 'confirmed', 'Admin@Admin.com', '$2y$10$4S2j8llFy4zSbExTvK5jh.EQByZZ2mTWyTLdBhok7ZB9hYKZkGxue', 'nvyxDXq6quGEOhreSVqEMcI6CYiidzK7OnFLzwvIu5N2GCRMctDqb56TZRLd', '2018-03-24 01:08:38', '2018-03-24 01:08:38'),
(31, 'Testuser', 'Dr', 'walid', 'USA', 'images/image_placeholder.png', 'confirmed', 'walid.johnson@gmail.com', 'password', '', '2020-03-24 14:12:43', '0000-00-00 00:00:00'),
(32, 'Walid Johnson', 'Mr.', 'walidjohnson', 'Shorewood', 'images/image_placeholder.png', NULL, 'wjohnson@fcainc.com', '$2y$10$FLgSLXi2xRqZdz71f0a1pO/eXXqf5xhg6uTjRokLaz7e.CVpluYo2', 'fZL4aXx35FyAN5wuxaz6xTRbVYA6Ddy1xqCn0qwgqU3rh0yQdfZhUY92dSEI', '2020-03-24 12:40:35', '2020-03-24 12:40:35'),
(33, 'Ernest Ellison', 'Physician Assistant', 'EEllison', 'Home Health Medical', 'images/image_placeholder.png', 'confirmed', 'Ernestellison@hotmail.com', '$2y$10$4S2j8llFy4zSbExTvK5jh.EQByZZ2mTWyTLdBhok7ZB9hYKZkGxue', '8KlKKoo2JnCiwUXb7hxdrLYNzjpZ6igU3hJwa1KrCsqmD9buC3k1gbIE6zEl', '2020-04-29 18:54:24', '2020-04-29 18:54:24'),
(34, '1', NULL, '1', '1', 'images/image_placeholder.png', NULL, 'snowman0707@outlook.com', '$2y$10$IPg2EOshFShIN4BospdLM.rX3yWgWF0CIfjCw7/O2FwMMtuQH6hUK', 'AEzQlBorVFxkXImlqBJOFyKhQ2aAd0vdGoRJvRdfbP0oaox17vpUBvJlo5Vc', '2020-07-21 19:02:18', '2020-07-21 19:02:18'),
(35, 'sysadmin', 'sysadmin', 'sysadmin', NULL, 'images/image_placeholder.png', NULL, 'sysadmin@admin.com', '$2y$10$YGtPKkWx8TWHYg5fnsWNguoEDoyB2DmEz6Rw52vJFmIwDOfKXcagS', 'e50znYJkItozmxcn9wpNKo3VZrWmBLodgoU0EcKh58gYd7wMnNlwCsQdsZR3', '2020-07-21 19:17:12', '2020-07-21 19:17:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `approvalofpatient`
--
ALTER TABLE `approvalofpatient`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `backupfiles`
--
ALTER TABLE `backupfiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `patientfiles`
--
ALTER TABLE `patientfiles`
  ADD PRIMARY KEY (`fileId`);

--
-- Indexes for table `patientprofile`
--
ALTER TABLE `patientprofile`
  ADD PRIMARY KEY (`PatientProfileId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `approvalofpatient`
--
ALTER TABLE `approvalofpatient`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `backupfiles`
--
ALTER TABLE `backupfiles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `patientfiles`
--
ALTER TABLE `patientfiles`
  MODIFY `fileId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patientprofile`
--
ALTER TABLE `patientprofile`
  MODIFY `PatientProfileId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
