// replace these values with those generated in your TokBox Account
var apiKey = "46712282";
var sessionId = "1_MX40NjcxMjI4Mn5-MTU4ODMxNTkxMTkxNn45TnN2UXN2Y20wTlJkR3BNaW9MNkxoNEh-fg";
var token = "T1==cGFydG5lcl9pZD00NjcxMjI4MiZzaWc9NDYxOWVlMDAxMTQ3YWM5ZWFiODBiYmEzYjVjYjA1YmM5NDdmNzRiYjpzZXNzaW9uX2lkPTFfTVg0ME5qY3hNakk0TW41LU1UVTRPRE14TlRreE1Ua3hObjQ1VG5OMlVYTjJZMjB3VGxKa1IzQk5hVzlNTmt4b05FaC1mZyZjcmVhdGVfdGltZT0xNTg4MzE1OTYwJm5vbmNlPTAuOTUxOTUyMTk5NjMxODM0NSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNTkwOTA3OTU3JmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9";


// Handling all of our errors here by alerting them
function handleError(error) {
  if (error) {
    alert(error.message);
  }
}


// (optional) add server code here
    var SERVER_BASE_URL = 'https://othhm.herokuapp.com';
    fetch(SERVER_BASE_URL + '/session').then(function(res) {
      return res.json()
    }).then(function(res) {
      apiKey = res.apiKey;
      sessionId = res.sessionId;
      token = res.token;
      initializeSession();
    }).catch(handleError);
  


function initializeSession() {
  var session = OT.initSession(apiKey, sessionId);

  // Subscribe to a newly created stream
session.on('streamCreated', function(event) {
  session.subscribe(event.stream, 'subscriber', {
    insertMode: 'append',
    width: '100%',
    height: '100%'
  }, handleError);
});

var sharingScreen = document.getElementById('start_sharing');

var stopsharingScreen = document.getElementById('stop_sharing');   
    
    sharingScreen.onclick = function(){
        stopsharingScreen.style.display = 'block';
        sharingScreen.style.display = 'none';
    OT.checkScreenSharingCapability(function(response) {
        if(!response.supported || response.extensionRegistered === false) {
            // This browser does not support screen sharing.
        } else if (response.extensionInstalled === false) {
            // Prompt to install the extension.
        } else {
            // Screen sharing is available. Publish the screen.
            var publisher = OT.initPublisher('screen-preview',
                {videoSource: 'screen'},
                function(error) {
                    if (error) {
                        // Look at error.message to see what went wrong.
                    } else {
                        session.publish(publisher, function(error) {
                            if (error) {
                                // Look error.message to see what went wrong.
                            }
                        });
                    }
                }
            );
        }
    });
};


  // Create a publisher
  var publisher = OT.initPublisher('publisher', {
    insertMode: 'append',
    width: '100%',
    height: '100%'
  }, handleError);

  // Connect to the session
  session.connect(token, function(error) {
    // If the connection is successful, publish to the session
    if (error) {
      handleError(error);
    } else {
      session.publish(publisher, handleError);
    }
  });
    session.on('streamCreated', function(event) {
        var subOptions = {
            appendMode: 'append'
        };

        var parentElementId = event.stream.videoType === 'screen' ?
            'sub-screen-sharing-container' :
            'sub-camera-container';
        subscriber = session.subscribe(event.stream, parentElement, subOptions);
    });
}

