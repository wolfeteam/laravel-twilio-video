<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Ajax Codes*/
Route::post('ajaxgetpatient','PatientController@ajaxgetpatient');
Route::post('ajaxgetpatient','PatientController@ajaxgetpatient');
Route::post('ajaxgetcallid','PatientController@ajaxgetcallid');
Route::post('ajaxUpdateSetupPatient', 'PatientController@ajaxUpdateSetupPatient');
Route::post('ajaxUpdateGetApproval', 'PatientController@ajaxUpdateGetApproval');
Route::post('ajaxUpdateCreateAppointment', 'PatientController@ajaxUpdateCreateAppointment');
Route::post('ajaxUpdateHaveMeeting', 'PatientController@ajaxUpdateHaveMeeting');


/*Home Routes*/
Route::get('/', 'HomepageController@login');
Route::get('home', 'HomeController@index');


Route::group(['middleware' => ['auth']], function () {
    Route::post('createRoom', 'ChatController@createRoom')->name('chat.createRoom');
    Route::post('joinVideoRoom', 'ChatController@joinVideoRoom')->name('chat.joinVideoRoom');
    Route::post('sendSMS', 'ChatController@sendSMS')->name('chat.sendSMS');
});

Route::get('room/join/{roomName}', 'ChatController@joinRoom');
Route::get('fullroom/join/{roomName}/{identity}', 'ChatController@fullJoinRoom');



Route::get('Import/{backupid}',function($backupid){
	// check if current user is owner of backup or not and then perform it 
	$backupfile = DB::table('backupfiles')->where([['id',$backupid]])->first();
	$backupfilename= $backupfile->link;
	if (isset($backupfile)){

		


		   DB::table('patientprofile')->delete();
		   $con=mysqli_connect("localhost","hms_patient","Admin@patient","hms_patientsys");
		   $table_name = "patientprofile";
		   $filepath  = "backupfiles/".$backupfilename.".sql";
   
		   if(! $con ) {
		      die('Could not connect: ' . mysql_error());
		   }

		   // start to perform each query 
		   $handle = fopen($filepath, "r");
			if ($handle) {
			    while (($line = fgets($handle)) !== false) {
			        // process the line read. and perform  each line alone to avoid large query execution time for large data 
			        //echo $line ; echo "<br><br>";
			        $retval = mysqli_query($con,$line);
				   if(! $retval ) {
				   	 //echo $contents;
				     die('Could not load data : ' . mysqli_error($con));
				   }
			    }

			    fclose($handle);
			} else {
			    // error opening the file.
			    echo "Error";
			} 

			mysqli_close($con);
		   	return back()->with('status', 'Patient Data has been Restored Successfully');
	}
	else {

		return back()->with('status', 'Sorry ! you didnt have premission to access this files');
	}
	

});

/*patient routs */
/*View all patients Page */
Route::get('viewpatient', 'PatientController@getUserPatients');
Route::post('addappointment', 'PatientController@addappointment')->name("addappointment");
/*add new patient page and function*/
Route::get('addpatientpage', 'HomeController@addpatientpage');
Route::post('addpatient', 'PatientController@addpatient');
/*edditpatient page and function*/
Route::get('editpatientpage/{id}', 'PatientController@editpatientpage');
Route::post('editpatient', 'PatientController@editpatient');
/*view and upload , and Delete patient files*/
Route::get('patientfiles/{id}' ,'PatientController@patientfiles');
Route::get('patientUpdate/{id}' ,'PatientController@patientupdate');

Route::post('uppatientfiles', 'PatientController@uppatientfiles');
Route::get('deletefile/{id}','PatientController@deletefile');
/*search patient function*/
Route::post('searchpatient','PatientController@searchpatient');
/*delete patient fnction*/
Route::get('deletepatient/{id}','PatientController@deletepatient');
/*delete backup file for patient fnction*/
Route::get('DeleteBackup/{id}','PatientController@DeleteBackup');


Route::get('apitest',function (){

require  base_path().'/AthenaHealth_api/athenahealthapi.php';
$key = 'k57rskp4hbaae44yk2hp5rq6';
$secret = 'dJYcnSStw7unRXj';
$version = 'preview1';
$practiceid = 195900;

$api = new APIConnection($version, $key, $secret, $practiceid);

$patient = $api->GET('/patients/',['firstname'=>'b','limit'=>50]);
echo "Custom fields:\n";
echo "<pre>";
print_r($patient);

});


Route::get('test',function (){

	$patient=DB::table('patientprofile')->where('PatientProfileId','2471')->first();
	$a = $patient->Phone1Type ;
	echo $a;
	echo "<br>";
	$b = str_replace("'", "\'", $a);
	echo $b;
});

Route::get('BackupMyPatients',function(){
	$PatientsCode = '';
	$InsertCode ='INSERT INTO `patientprofile` (`PatientProfileId`, `Prefix`, `First`, `Middle`, `Last`, `Suffix`, `Nickname`, `Address1`, `Address2`, `City`, `State`, `Zip`, `Country`, `AddressType`, `Phone1`, `Phone1Type`, `Phone2`, `Phone2Type`, `Phone3`, `Phone3Type`, `County`, `EMailAddress`, `AlternateAddress1`, `AlternateAddress2`, `AlternateCity`, `AlternateState`, `AlternateZip`, `AlternateCounty`, `AlternateCountry`, `AlternateAddressType`, `SchoolName`, `SSN`, `Birthdate`, `DeathDate`, `Sex`, `ReferredByPatientId`, `PatientSameAsGuarantor`, `MedicalRecordNumber`, `ProfileNotes`, `AlertNotes`, `AppointmentNotes`, `BillingNotes`, `Picture`, `visdocnum`, `SSDID`, `Created`, `LastModified`, `RegNote`) VALUES';

	$Patients = DB::table('patientprofile')->get();
	$count = $Patients->count();
	foreach ($Patients as $Patient) {

		//replace new line break for all vaiables
		$Patient->PatientProfileId = preg_replace("/\n/m", '\n', $Patient->PatientProfileId);
		$Patient->Prefix = preg_replace("/\n/m", '\n', $Patient->Prefix);
		$Patient->First = preg_replace("/\n/m", '\n', $Patient->First);
		$Patient->Middle = preg_replace("/\n/m", '\n', $Patient->Middle);
		$Patient->Last = preg_replace("/\n/m", '\n', $Patient->Last);
		$Patient->Suffix = preg_replace("/\n/m", '\n', $Patient->Suffix);
		$Patient->Nickname = preg_replace("/\n/m", '\n', $Patient->Nickname);
		$Patient->Address1 = preg_replace("/\n/m", '\n', $Patient->Address1);
		$Patient->Address2 = preg_replace("/\n/m", '\n', $Patient->Address2);
		$Patient->City = preg_replace("/\n/m", '\n', $Patient->City);
		$Patient->State = preg_replace("/\n/m", '\n', $Patient->State);
		$Patient->Zip = preg_replace("/\n/m", '\n', $Patient->Zip);
		$Patient->Country = preg_replace("/\n/m", '\n', $Patient->Country);
		$Patient->AddressType = preg_replace("/\n/m", '\n', $Patient->AddressType);
		$Patient->Phone1 = preg_replace("/\n/m", '\n', $Patient->Phone1);
		$Patient->Phone1Type = preg_replace("/\n/m", '\n', $Patient->Phone1Type);
		$Patient->Phone2 = preg_replace("/\n/m", '\n', $Patient->Phone2);
		$Patient->Phone2Type = preg_replace("/\n/m", '\n', $Patient->Phone2Type);
		$Patient->Phone3 = preg_replace("/\n/m", '\n', $Patient->Phone3);
		$Patient->Phone3Type = preg_replace("/\n/m", '\n', $Patient->Phone3Type);
		$Patient->County = preg_replace("/\n/m", '\n', $Patient->County);
		$Patient->EMailAddress = preg_replace("/\n/m", '\n', $Patient->EMailAddress);
		$Patient->AlternateAddress1 = preg_replace("/\n/m", '\n', $Patient->AlternateAddress1);
		$Patient->AlternateAddress2 = preg_replace("/\n/m", '\n', $Patient->AlternateAddress2);
		$Patient->AlternateCity = preg_replace("/\n/m", '\n', $Patient->AlternateCity);
		$Patient->AlternateState = preg_replace("/\n/m", '\n', $Patient->AlternateState);
		$Patient->AlternateZip = preg_replace("/\n/m", '\n', $Patient->AlternateZip);
		$Patient->AlternateCounty = preg_replace("/\n/m", '\n', $Patient->AlternateCounty);
		$Patient->AlternateCountry = preg_replace("/\n/m", '\n', $Patient->AlternateCountry);
		$Patient->AlternateAddressType = preg_replace("/\n/m", '\n', $Patient->AlternateAddressType);
		$Patient->SchoolName = preg_replace("/\n/m", '\n', $Patient->SchoolName);
		$Patient->SSN = preg_replace("/\n/m", '\n', $Patient->SSN);
		$Patient->Birthdate = preg_replace("/\n/m", '\n', $Patient->Birthdate);
		$Patient->DeathDate = preg_replace("/\n/m", '\n', $Patient->DeathDate);
		$Patient->Sex = preg_replace("/\n/m", '\n', $Patient->Sex);
		$Patient->ReferredByPatientId = preg_replace("/\n/m", '\n', $Patient->ReferredByPatientId);
		$Patient->PatientSameAsGuarantor = preg_replace("/\n/m", '\n', $Patient->PatientSameAsGuarantor);
		$Patient->MedicalRecordNumber = preg_replace("/\n/m", '\n', $Patient->MedicalRecordNumber);
		$Patient->ProfileNotes = preg_replace("/\n/m", '\n', $Patient->ProfileNotes);
		$Patient->AlertNotes = preg_replace("/\n/m", '\n', $Patient->AlertNotes);
		$Patient->AppointmentNotes = preg_replace("/\n/m", '\n', $Patient->AppointmentNotes);
		$Patient->BillingNotes = preg_replace("/\n/m", '\n', $Patient->BillingNotes);
		$Patient->Picture = preg_replace("/\n/m", '\n', $Patient->Picture);
		$Patient->visdocnum = preg_replace("/\n/m", '\n', $Patient->visdocnum);
		$Patient->SSDID = preg_replace("/\n/m", '\n', $Patient->SSDID);
		$Patient->Created = preg_replace("/\n/m", '\n', $Patient->Created);
		$Patient->LastModified = preg_replace("/\n/m", '\n', $Patient->LastModified);
		$Patient->RegNote = preg_replace("/\n/m", '\n', $Patient->RegNote);
//replace (') for all vaiables to ('\)
		$Patient->PatientProfileId =str_replace("'", "\'",  $Patient->PatientProfileId); 
		$Patient->Prefix = str_replace("'", "\'", $Patient->Prefix);
		$Patient->First = str_replace("'", "\'", $Patient->First);
		$Patient->Middle = str_replace("'", "\'", $Patient->Middle);
		$Patient->Last = str_replace("'", "\'", $Patient->Last);
		$Patient->Suffix = str_replace("'", "\'", $Patient->Suffix);
		$Patient->Nickname = str_replace("'", "\'", $Patient->Nickname);
		$Patient->Address1 = str_replace("'", "\'", $Patient->Address1);
		$Patient->Address2 = str_replace("'", "\'", $Patient->Address2);
		$Patient->City = str_replace("'", "\'", $Patient->City);
		$Patient->State = str_replace("'", "\'", $Patient->State);
		$Patient->Zip = str_replace("'", "\'", $Patient->Zip);
		$Patient->Country = str_replace("'", "\'", $Patient->Country);
		$Patient->AddressType = str_replace("'", "\'", $Patient->AddressType);
		$Patient->Phone1 = str_replace("'", "\'", $Patient->Phone1);
		$Patient->Phone1Type = str_replace("'", "\'", $Patient->Phone1Type);
		$Patient->Phone2 = str_replace("'", "\'", $Patient->Phone2);
		$Patient->Phone2Type = str_replace("'", "\'", $Patient->Phone2Type);
		$Patient->Phone3 = str_replace("'", "\'", $Patient->Phone3);
		$Patient->Phone3Type = str_replace("'", "\'", $Patient->Phone3Type);
		$Patient->County = str_replace("'", "\'", $Patient->County);
		$Patient->EMailAddress = str_replace("'", "\'", $Patient->EMailAddress);
		$Patient->AlternateAddress1 = str_replace("'", "\'", $Patient->AlternateAddress1);
		$Patient->AlternateAddress2 = str_replace("'", "\'", $Patient->AlternateAddress2);
		$Patient->AlternateCity = str_replace("'", "\'", $Patient->AlternateCity);
		$Patient->AlternateState = str_replace("'", "\'", $Patient->AlternateState);
		$Patient->AlternateZip = str_replace("'", "\'", $Patient->AlternateZip);
		$Patient->AlternateCounty = str_replace("'", "\'", $Patient->AlternateCounty);
		$Patient->AlternateCountry = str_replace("'", "\'", $Patient->AlternateCountry);
		$Patient->AlternateAddressType = str_replace("'", "\'", $Patient->AlternateAddressType);
		$Patient->SchoolName = str_replace("'", "\'", $Patient->SchoolName);
		$Patient->SSN = str_replace("'", "\'", $Patient->SSN);
		$Patient->Birthdate = str_replace("'", "\'", $Patient->Birthdate);
		$Patient->DeathDate = str_replace("'", "\'", $Patient->DeathDate);
		$Patient->Sex = str_replace("'", "\'", $Patient->Sex);
		$Patient->ReferredByPatientId = str_replace("'", "\'", $Patient->ReferredByPatientId);
		$Patient->PatientSameAsGuarantor = str_replace("'", "\'", $Patient->PatientSameAsGuarantor);
		$Patient->MedicalRecordNumber = str_replace("'", "\'", $Patient->MedicalRecordNumber);
		$Patient->ProfileNotes = str_replace("'", "\'", $Patient->ProfileNotes);
		$Patient->AlertNotes = str_replace("'", "\'", $Patient->AlertNotes);
		$Patient->AppointmentNotes = str_replace("'", "\'", $Patient->AppointmentNotes);
		$Patient->BillingNotes = str_replace("'", "\'", $Patient->BillingNotes);
		$Patient->Picture = str_replace("'", "\'", $Patient->Picture);
		$Patient->visdocnum = str_replace("'", "\'", $Patient->visdocnum);
		$Patient->SSDID = str_replace("'", "\'", $Patient->SSDID);
		$Patient->Created = str_replace("'", "\'", $Patient->Created);
		$Patient->LastModified = str_replace("'", "\'", $Patient->LastModified);
		$Patient->RegNote = str_replace("'", "\'", $Patient->RegNote);
		
			$PatientsCode = $PatientsCode.$InsertCode."(".$Patient->PatientProfileId.",'".$Patient->Prefix."','".$Patient->First."','".$Patient->Middle."','".$Patient->Last."','".$Patient->Suffix."','".$Patient->Nickname."','".$Patient->Address1."','".$Patient->Address2."','".$Patient->City."','".$Patient->State."','".$Patient->Zip."','".$Patient->Country."','".$Patient->AddressType."','".$Patient->Phone1."','".$Patient->Phone1Type."','".$Patient->Phone2."','".$Patient->Phone2Type."','".$Patient->Phone3."','".$Patient->Phone3Type."','".$Patient->County."','".$Patient->EMailAddress."','".$Patient->AlternateAddress1."','".$Patient->AlternateAddress2."','".$Patient->AlternateCity."','".$Patient->AlternateState."','".$Patient->AlternateZip."','".$Patient->AlternateCounty."','".$Patient->AlternateCountry."','".$Patient->AlternateAddressType."','".$Patient->SchoolName."','".$Patient->SSN."','".$Patient->Birthdate."','".$Patient->DeathDate."','".$Patient->Sex."','".$Patient->ReferredByPatientId."','".$Patient->PatientSameAsGuarantor."','".$Patient->MedicalRecordNumber."','".$Patient->ProfileNotes."','".$Patient->AlertNotes."','".$Patient->AppointmentNotes."','".$Patient->BillingNotes."','".$Patient->Picture."','".$Patient->visdocnum."','".$Patient->SSDID."','".$Patient->Created."','".$Patient->LastModified."','".$Patient->RegNote."');"."\n";
			
		}
	$User_id = 	Auth::user()->id;
	$username = Auth::user()->username;
	$filename = rand().$username."-Backup";
	$fullpath = 'backupfiles/'.$filename.'.sql';
	DB::table('backupfiles')->insert([
		'link' => $filename,
	]);
	File::put($fullpath,$PatientsCode);
	return back()->with('status', 'Patient Data Backup has been Created Successfully !');




});
	
/*chat*/
Route::get('appointment', 'ChatController@appointment');
Route::get('callpatient', 'ChatController@chat');
Route::post('callpatient', 'ChatController@chat')->name('callpatient');
Route::get('file', 'FileController@index');
Route::post('fileUpload', 'FileController@uploadFile');

/*Send Email and SMS*/
// Route::post('sendemail', 'HomeController@sendemail');
// Route::post('sendsms', 'HomeController@sendsms');

/*View all patients Page */
Route::get('setup', 'HomeController@setup');
/*change password*/
Route::post('chgpwd', 'HomeController@chgpwd');
Route::post('DeleteAccount', 'HomeController@DeleteAccount');


Auth::routes();

/*to logout */
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

