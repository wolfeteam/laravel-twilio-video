-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2018 at 10:59 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `patients`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/image_placeholder.png',
  `confirmed` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `title`, `username`, `hospital`, `image`, `confirmed`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'User', 'Dr', 'User', 'USA', 'images/image_placeholder.png', NULL, 'User@example.com', '$2y$10$UMR3nxvNPDHCst3gQDl.NO3dhYp0JE1hiqY5LbFcZcp5l6FX/rPBO', NULL, '2018-03-04 23:12:43', '2018-03-04 23:12:43'),
(20, 'Tito', 'Tito', 'Tito', 'Egypt', 'images/image_placeholder.png', NULL, 'test@tremno.com', '$2y$10$UMR3nxvNPDHCst3gQDl.NO3dhYp0JE1hiqY5LbFcZcp5l6FX/rPBO', 'HwCUm6G5G7OB6rLHpzt5pPfnyx7RruuziTbCYmBGUapx3tyg2JWd5I1rBXhf', '2018-03-04 23:12:43', '2018-03-04 23:12:43'),
(21, 'Titos', 'Titos', 'Titos', 'Egypts', 'images/image_placeholder.png', NULL, 'tests@tremno.com', '$2y$10$UMR3nxvNPDHCst3gQDl.NO3dhYp0JE1hiqY5LbFcZcp5l6FX/rPBO', NULL, '2018-03-04 23:12:43', '2018-03-04 23:12:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
