@extends('app')
@section ('title')
<title>Telemed | Edit Patient</title>
@stop
@section('menu')
<li><a href="{{url('home')}}" >HOME</a></li>
<li class="active"><a href="{{url('viewpatient')}}">PATIENTS</a></li>
<li><a href="{{url('callpatient')}}">CALL</a></li>
<li><a  href="{{url('setup')}}">SETUP</a></li>
@stop
@section ('content')
<script>
   var loadFile = function(event) {
     var output = document.getElementById('output');
     output.src = URL.createObjectURL(event.target.files[0]);
   };
</script>
<div class="container-fluid">
   <div class="row">
      <div class="col-md-3">
         <ul class="list-group text-right">
            <li class="list-group-item "><a href="{{url('viewpatient')}}" >View Patients</a></li>
            <li class="list-group-item"><a href="{{url('addpatientpage')}}" class="left-nav-active">Edit Patient</a></li>
            <!--<li class="list-group-item"><a href="">Edit Patient Data</a></li>-->
         </ul>
      </div>
      <div class="col-md-7 lft-part">
         <div class="row">
            <div class="col-md-12">
               <div class="row">
                  @if (session('status'))
                  <div id="alertmsg" class="alert alert-success">
                     {{ session('status') }}
                  </div>
                  @endif
                  <form class="form-signin" id="register-form" method="post" enctype="multipart/form-data" action="{{url('editpatient')}}">
                     <h2 class="form-signin-heading">Edit Patient</h2>
                     <div class="col-md-12">
                        <div class="row">
                           <div class="row">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <input type="hidden" name="patientid" value="{{ $patient['patientid'] }}">
                              
                              <div class="col-md-6">
                                 <div class="col-md-12">
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>First Name *</label>
                                       <input type="text" class="form-control" value="{{$patient['firstname']}}" placeholder="First Name *" name="First"   required />
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label>Last Name *</label>
                                       <input type="text" class="form-control" value="{{$patient['lastname']}}" placeholder="Last Name *"  name="Last"  required  />
                                    </div>
                                 </div>
                                 <div class="col-md-12">
                                    <br>
                                    <div class="form-group radio-label">
                                       <label>Sex</label>            
                                       <input class="custom-radio" type="radio" checked="checked" @if($patient["sex"] =='M')  checked="checked" @endif name="sex" id="3" value="Male">
                                       <label for="3"> Male</label>
                                       <input class="custom-radio" @if($patient["sex"] =='F')  checked="checked" @endif type="radio" name="sex" id="4" value="Female">
                                       <label for="4"> Female</label>
                                    </div>
                                    <br>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group"> 
                                          <img src="{{url('images/noimage.jpg')}}}}" id="output" alt="" class="pic-pp"> 
                                       </div>
                                    </div>
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <!--<input type="file" name="image" id="image">-->
                                          <label class="btn btn-primary btn-cus"> Upload Photo
                                          <input type="file"  onchange="loadFile(event)"  name="photo" id="image" style="display: none;">
                                          </label>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="col-md-12">
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label>Country *</label>
                                 <select class="form-control"  name="Country"  required>
                                    <option value="" selected="selected"  >-- Select Country -- </option>
                                    <option  @if($patient['countrycode'] =="USA") selected @endif  value="USA">United States</option> 
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label>Mobile Phone</label>
                                 <input type="number" class="form-control"  value="{{ array_key_exists('mobilephone',$patient)?$patient['mobilephone']:''}}"  placeholder="Mobile number *" name="mobilephone"   required />
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>PhoneCarrier *</label>
                                 <select class="form-control" id="PhoneCarrier" name="mobilecarrierid" required>
                                    <option value="">-- Select Phone Carrier --</option>
                                    <option value="21" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="21") selected @endif>ACS Wireless</option>
                                    <option value="22" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:"" =="22") selected @endif>Alltel</option>
                                    <option value="24" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="24") selected @endif>AT&T Cingular</option>
                                    <option value="26" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="26") selected @endif>Boost (iDEN)</option>
                                    <option value="34" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="34") selected @endif>Cincinnati Bell</option>
                                    <option value="35" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="35") selected @endif>Cricket Communincations</option>
                                    <option value="81" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="81") selected @endif>MetroPCS</option>
                                    <option value="58" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="58") selected @endif>SaskTel Mobility</option>
                                    <option value="36" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="36") selected @endif>GCI Communications</option>
                                    <option value="38" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="38") selected @endif>Illinois Valley Cellular</option>
                                    <option value="37" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="37") selected @endif>Golden State Cellular</option>
                                    <option value="59" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="59") selected @endif>Telus Mobility</option>
                                    <option value="49" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="49") selected @endif>US Cellular</option>
                                    <option value="50" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="50") selected @endif>Verizon Wireless</option>
                                    <option value="60" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="60") selected @endif>Virgin Mobile</option>
                                    <option value="57" @if (array_key_exists("mobilecarrierid",$patient)?$patient['mobilecarrierid']:""=="57") selected @endif>Rogers Wireless</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>EMailAddress</label>
                                 <input type="text" class="form-control"  value="{{array_key_exists('email',$patient)?$patient['email']:''}}"  placeholder="Email Address" name="email"    />
                              </div>
                           </div>
                           <div class="col-md-6 ">
                              <div class="form-group">
                                 <label>Birthdate</label>
                                 <?php
                                    $dt1 = new DateTime($patient['dob']);
                                    $Birthdate = $dt1->format('Y-m-d');
                                    
                            
                                    
                                    ?>
                                 <input type="date" class="form-control"   value="{{$Birthdate}}"  name="dob"   id="Birthdate" />
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Profile Notes</label>
                                 <textarea class="form-control"   value="" placeholder="Profile Notes" id="ProfileNotes" name="ProfileNotes"   ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Alert Notes </label>
                                 <textarea class="form-control"   value="" placeholder="Alert Notes" id="AlertNotes" name="AlertNotes"  ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Appointment Notes </label>
                                 <textarea class="form-control"   value="" placeholder="Appointment Notes" id="AppointmentNotes" name="AppointmentNotes"   ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Billing Notes</label>
                                 <textarea class="form-control"  value=""  placeholder="Billing Notes" id="BillingNotes" name="BillingNotes"   ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>Reg Note</label>
                                 <textarea class="form-control"  value=""  placeholder="Reg Note" id="RegNote" name="RegNote"   ></textarea>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                 <button type="submit" class="btn btn-default btn-green" name="btn-signup">Update Patient Data </button>
                              </div>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
         </div>
         </form>
      </div>
   </div>
</div>
</div>
</div>
</div>
@stop