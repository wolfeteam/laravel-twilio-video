@extends('layouts.public')
@section ('title')
<title>Telemed | Call Patient</title>
@stop
<head>
   <meta name="msapplication-tap-highlight" content="no">
   <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
   <meta name="description" content="Audio+Video+Screen Sharing using RTCMultiConnection" />
   <meta name="keywords" content="WebRTC,RTCMultiConnection,Demos,Experiments,Samples,Examples" />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <style>
      * {
      word-wrap:break-word;
      }
      video {
      object-fit: fill;
      width: 30%;
      }
      button,
      input,
      select {
      font-weight: normal;
      padding: 2px 4px;
      text-decoration: none;
      display: inline-block;star
      text-shadow: none;
      font-size: 16px;
      outline: none;
      }
      .make-center {
      text-align: center;
      padding: 5px 10px;
      }
      img, input, textarea {
      max-width: 100%
      }
      @media all and (max-width: 500px) {
      .fork-left, .fork-right, .github-stargazers {
      display: none;
      }
      }
      input{
      margin: auto;
      width: 50% ;
      text-align: center;
      }
      .chat-output{
      font-family: arial;
      font-size:18px;
      text-align: left;
      }
      .side_call_board{
          min-height: 150px;
          background: black;
      }
      .main_call_board{
          min-height: 500px;
          background: black;
      }
   </style>
</head>
 
@section('menu')
@if (Auth::user())
<li><a href="home" >HOME</a></li>
<li ><a href="viewpatient">PATIENTS</a></li>
<li class="active"><a href="callpatient">CALL</a></li>
<li ><a href="setup">SETUP</a></li>
@endif
@stop
@section ('content')
<div id="patientfiles1"></div>
<div class="container-fluid">
<div class="">
   <div class="col-md-3">
      <ul class="list-group text-right">
         <li class="list-group-item "><a href="#" class="left-nav-active">Make Call</a></li>
         {{-- 
         <li class="list-group-item"><a href="">Edit Patient Data</a></li>
         --}}
      </ul>
      
      <div style="padding: 10px">
         <h4 class="text text-success">Appointments</h4>
         <div id="appointment">
            @if(count($appointment) > 0)
            <div class="card" style="width: 100%;">
               <hr>
               <div class="card-body">
                  <h4 class="card-title">{{ $appointment['patientname'] }}</h4>
                  <h5 class="card-subtitle mb-2 text-muted"><span class="text text-primary">Email:</span><p id="ppatientEmail">{{ $appointment['patientemail'] }}</p></h5>
                  <h5 class="card-subtitle mb-2 text-muted"><span class="text text-primary">Phone:</span><p id="ppatiendNumber">{{ $appointment['mobilephone'] }}</p></h5>
                  <h5 class="card-subtitle mb-2 text-muted"><span class="text text-primary">DateTime:</span> {{ $appointment['appointment_datetime'] }} </h5>
                  <!--<p class="card-text"> </p>-->
                  <div>
                     <input type="hidden" value="{{ $appointment['patientid'] }}">
                     <a data-target="#submitModal" data-toggle="modal" class="select-for-call btn btn-info" id="selectMe">select me</a>
                     <!--<a class="select-for-call btn btn-info" id="selectMe">select me</a>-->
                  </div>
               </div>
            </div>
            @endif
         </div>
      </div>
   </div>
   <div class="col-md-9" style="overflow-x:auto;">
      <section class="experiment">
         
            @if (session('status'))
            <div id="alertmsg" class="alert alert-success">
               {{ session('status') }}
            </div>
            @endif
            @if(count($appointment) > 0)
            <div class="row">
               <ul class="nav nav-pills">
                  <li class="active"><a data-toggle="pill" href="#sms_pill" class="start_btns btn">Send Notification</a></li>
                  <li class=""><a href="https://video-app-3508-1828-dev.twil.io?passcode=03803235081828" target="window" class="start_btns btn btn-default" id="start_appointment_pill_btn">Start Appointment</a></li>
                  <li class=""><a href="https://athena.hhmtelemedicine.com/appointment">Start Appointment</a></li>
               </ul>
               <div class="tab-content">
                  <div id="sms_pill" class="tab-pane fade in active">
                     <div class="col-md-8">
                        <h4>Send Patient Telemedicine Link.</h4>
                        <div class="notfication_message_box container">
                           <div class="col=md-12">
                              <textarea class="jumbotron" id="sms_msg" rows="5" cols="150" value="Connect with Dr. Sam via TeleHealth. Click the link. <a href='https://video-app-3576-2305-dev.twil.io?passcode=52170735762305'>Link</a> to start and enter room 101 and press join call.">
Connect with Dr. Sam via TeleHealth. Click the link. <a href='https://video-app-3508-1828-dev.twil.io?passcode=03803235081828'>Link</a> to start and enter room 101 and press join call.</textarea>
                              <div class="pull-right">
                                 <button class="btn btn-primary hidden">Save Message</button>
                              </div>
                           </div>
                        </div>
                        <br />
                        <div class="row">
                           <div class="col-md-12">
                              <input type="checkbox" class="col-md-2" />
                              <input class="col-md-10" type="text" id="patientpatientemail" placeholder="Enter Email" value="" />
                              <hr/>
                              @if (Auth::user())
                              <button  class="btn btn-primary pull-right">Send Email</button>
                              @endif
                           </div>
                        </div>
                        <br />
                        <div class="row">
                           <div class="col-md-12">
                              <input type="checkbox" class="col-md-2" />
                              <input class="col-md-10" type="text" id="patientphoneNum" placeholder="Enter Mobile Number" value="" />
                              <hr/>
                              @if (Auth::user())
                              <button  class="btn btn-primary pull-right sendSMS">Send Sms</button>
                              <button class="btn btn-info pull-right hidden" id="sms_status_btn">SMS Message Sent!</button>
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="video_pill" class="tab-pane fade">
                      <div class="col-md-8">
                          <div class="col-md-12">
                            <label>Name: </label><input type="text" value="{{ $appointment['patientname'] }}" /><button class="btn btn-primary">Start Call</button>
                          </div>
                          <div class="col-md-4">
                              <div class="side_call_board"></div>
                              <hr>
                              <div class="side_call_board"></div>
                          </div>
                          <div class="col-md-8">
                              <div class="main_call_board"></div>
                          </div>
                      </div>
                  </div>
                  <br>
               </div>
               @endif       
            </div>
      </section>
        
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="submitModal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Create Video Room</h4>
         </div>
         @if(count($appointment) > 0)
         <div class="modal-body">
            <div class="form-group">
               <label class="">Room Name</label>
               <input type="text" name="roomName" id="roomNameS" />
               <input type="hidden" name="appointmentId" id="appointmentIdS" value="{{ $appointment['patientid'] }}" />
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-success" id="creatVideoRoom">Create</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
         @endif
      </div>
   </div>
</div>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
 
<script type="text/javascript" src="https://code.jquery.com/jquery-1.4.3.min.js" ></script>
<script>
    $(document).ready(function() {
        // creatVideoRoom();
    })
    function creatVideoRoom() {
        var roomName = $('#roomNameS').val()
       var appointmentId = $('#appointmentIdS').val()
       $.ajax({
         type:'POST',
         url:"{{ route('chat.createRoom')}}",
         data:{'roomName': roomName, 'appointmentId': appointmentId, '_token': $('input[name=_token]').val()},
         success:function(data){
          $("#submitModal").modal('hide')
           if(data.status) {
               var html = 'Connect with Dr.Sam via TeleHealth. Click the link to start and enter room. \n';
               //   html += "<a href='https://video-app-"+data.roomName+"?passcode=1965426'>Link</a> \n";
            //   html += "<a href='https://video-app-1745-dev.twil.io?passcode=3192571745'>Link</a> \n";
                html += "<a href='"+data.roomName+"'>Link</a> \n or redirect to this link \n";
                html += data.roomName;
               $('#sms_msg').html(html)
               // $('#selectMe').addClass("hidden")
               // $('.start_btns').removeClass('hidden')
               var patientEmail = $("#ppatientEmail").text()
               $("#patientpatientemail").val(patientEmail)
               var patientNumber = $("#ppatiendNumber").text()
               $("#patientphoneNum").val(patientNumber)
           }else
                alert(data.error_msg);
           // completeStatus = 1;
         }
       });
    }
  $('#creatVideoRoom').click(function() {
      creatVideoRoom();
  })
</script>
<script>
   $('.sendSMS').click(function() {
       var phoneNum = $('#patientphoneNum').val()
       var message  = $('#sms_msg').val()
       if(!phoneNum) {
           alert("Please enter Mobile Number");
           return false;
       }
       var placceholder = "Here will be the message to send patient with video room link.";
       if(!message || message == placceholder) {
           alert("Please enter Message");
           return false;
       }
       $.ajax({
         type:'POST',
         url:"{{ route('chat.sendSMS')}}",
         data:{'phoneNum': phoneNum, 'message': message, '_token': $('input[name=_token]').val()},
         success:function(data){
           if(data.status) {
              $("#sms_status_btn").removeClass("hidden")
              $("#start_appointment_pill_btn").removeClass("disabled")
           }
           
         }
       });
   })
</script>
@stop