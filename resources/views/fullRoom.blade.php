<html>
<head>
<title>Telemed | Call Patient</title>
<link rel="stylesheet" type="text/css" href="{{ url('/css/bootstrap.min.css')}}">
<script src="//media.twiliocdn.com/sdk/js/video/v1/twilio-video.min.js"></script>
<script>
    var countUser = 0;
    var countParticipants = 0;
    Twilio.Video.createLocalTracks({
       audio: true,
       video: { width:window.innerWidth }
    }).then(function(localTracks) {
       return Twilio.Video.connect('{{ $accessToken }}', {
           name: '{{ $roomName }}',
           tracks: localTracks,
           video: { width:window.innerWidth }
       });
    }).then(function(room) {
       console.log('Successfully joined a Room: ', room);
       countParticipants = room.participants.size + 1;
       room.participants.forEach(participantConnected);

       var previewContainer = document.getElementById(room.localParticipant.sid);
       if (!previewContainer || !previewContainer.querySelector('video')) {
           participantConnected(room.localParticipant);
       }

       room.on('participantConnected', function(participant) {
           console.log("Joining: '" +  participant.identity +  "'");
           participantConnected(participant);
       });

       room.on('participantDisconnected', function(participant) {
           console.log("Disconnected: '"  + participant.identity +  "'");
           participantDisconnected(participant);
       });
    });
    // additional functions will be added after this point
    function participantConnected(participant) {
       console.log('Participant "%s" connected', participant.identity);
       countUser++;
       if(countUser>6 && countUser != countParticipants) return;
       const div = document.createElement('div');
       div.id = participant.sid;
       div.setAttribute("style", "float: left; margin: 10px;");
       div.innerHTML = "<span style='z-index: 3;position: relative;background: rgb(27 47 64 / 20%);padding: 7px 10px;margin: 10px;top: 10px;'>"+ participant.identity+ "</span>";
    
       participant.tracks.forEach(function(track) {
           trackAdded(div, track)
       });
    
       participant.on('trackAdded', function(track) {
           trackAdded(div, track)
       });
       participant.on('trackRemoved', trackRemoved);       
       if(countUser == countParticipants){
        div.setAttribute("style","width:100%;position:absolute;z-index:1;height:100%")
        document.getElementById('media-div').appendChild(div);        
       }
       else{ 
         div.setAttribute("style","position:relative;z-index:2;height: 150px;width: 225px;padding: 4px;border: 3px solid;border-radius: 12px;margin: 3px 0;")
        document.getElementById('others').appendChild(div);
       }
       
    }

function participantDisconnected(participant) {
   console.log('Participant "%s" disconnected', participant.identity);

   participant.tracks.forEach(trackRemoved);
   document.getElementById(participant.sid).remove();
}
function trackAdded(div, track) {
   div.appendChild(track.attach());
   var video = div.getElementsByTagName("video")[0];
   if (video) {
     
      video.setAttribute("style", "width:100%;height:100%;object-fit:cover;margin-top:-20px;");

   }
}

function trackRemoved(track) {
   track.detach().forEach( function(element) { element.remove() });
}
</script>
</head> 
 <body>
  <div class="content">    
      <div > 
          <div id="media-div" >
          <div id="others" style="position:absolute" class="col-md-2 col-md-offset-10"></div>
          </div>        
    </div>
  </div> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>    
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>  
</body>
</html>