@extends('app')

@section ('title')
    <title>Telemed | Add Patient</title>
@stop

@section('menu')
<li><a href="home" >HOME</a></li>
<li class="active"><a href="viewpatient">PATIENTS</a></li>
<li><a href="callpatient">CALL</a></li>
<li><a  href="{{url('setup')}}">SETUP</a></li>
@stop


@section ('content')
<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>


    <script>
$('.nav').on('click', 'li', function(){
    $('.nav li').removeClass('active');
    $(this).addClass('active');
});
</script> 



<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <ul class="list-group text-right">
        <li class="list-group-item "><a href="viewpatient" >View / Edit Patients</a></li>
                <li class="list-group-item"><a href="addpatientpage" class="left-nav-active">Add Patient</a></li>
        <!--<li class="list-group-item"><a href="">Edit Patient Data</a></li>-->
      </ul>
    </div>
    <div class="col-md-7 lft-part">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <form class="form-signin" id="register-form" method="post" enctype="multipart/form-data" action="addpatient">
              <h2 class="form-signin-heading">Add Patient</h2>
                


                <div class="col-md-12">
                <div class="row">
                <div class="row">

                 <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="col-md-6">
                      <div class="col-md-12">
                      <div class="form-group">
                        <select class="form-control" name="Prefix">
                                  <option value="">-- Select Prefix --</option>
                                  <option value="1st Lt">1st Lt</option>
                                  <option value="Adm">Adm</option>
                                  <option value="Atty">Atty</option>
                                  <option value="Brother">Brother</option>
                                  <option value="Capt">Capt</option>
                                  <option value="Chief">Chief</option>
                                  <option value="Cmdr">Cmdr</option>
                                  <option value="Col">Col</option>
                                  <option value="Dean">Dean</option>
                                  <option value="Dr">Dr</option>
                                  <option value="Elder">Elder</option>
                                  <option value="Father">Father</option>
                                  <option value="Gen">Gen</option>
                                  <option value="Gov">Gov</option>
                                  <option value="Hon">Hon</option>
                                  <option value="Lt Col">Lt Col</option>
                                  <option value="Maj">Maj</option>
                                  <option value="MSgt">MSgt</option>
                                  <option value="Mr">Mr</option>
                                  <option value="Mrs">Mrs</option>
                                  <option value="Ms">Ms</option>
                                  <option value="Prince">Prince</option>
                                  <option value="Prof">Prof</option>
                                  <option value="Rabbi">Rabbi</option>
                                  <option value="Rev">Rev</option>
                                  <option value="Sister">Sister</option>
                                </select>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="First Name *" name="First"   required />
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Middle Name"  name="Middle"    />
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Last Name *"  name="Last"  required  />
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <select class="form-control" name="Suffix">
                                  <option value="">-- Select Suffix --</option>
                                  <option value="II">II</option>
                                  <option value="III">III</option>
                                  <option value="IV">IV</option>
                                  <option value="CPA">CPA</option>
                                  <option value="DDS">DDS</option>
                                  <option value="Esq">Esq</option>
                                  <option value="JD">JD</option>
                                  <option value="Jr">Jr</option>
                                  <option value="LLD">LLD</option>
                                  <option value="MD">MD</option>
                                  <option value="PhD">PhD</option>
                                  <option value="Ret">Ret</option>
                                  <option value="RN">RN</option>
                                  <option value="Sr">Sr</option>
                                  <option value="DO">DO</option>
                                </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nickname" name="Nickname"    />
                      </div>
                    </div>

                       <div class="col-md-12">
                              <br>
                              <div class="form-group radio-label">
                                <label>Sex</label>            
                               <input class="custom-radio" type="radio" checked="checked" name="sex" id="3" value="Male">
                               <label for="3"> Male</label>
                               <input class="custom-radio" type="radio" name="sex" id="4" value="Female">
                               <label for="4"> Female</label>
                              </div>
                               <br>
                            </div>

                     <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Address-1 *" name="Address1"  required  />
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Address-2" name="Address2"    />
                      </div>
                    </div>


                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="City *" name="City"   required />
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="State *" name="State" required  />
                      </div>
                    </div>

                  </div>

                  <div class="col-md-6">
                          
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group"> 
                                                          <img src="images/image_placeholder.png" id="output" alt="" class="pic-pp"> 
                                    
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group"> 
                                    <!--<input type="file" name="image" id="image">-->
                                    <label class="btn btn-primary btn-cus"> Upload Photo
                                      <input type="file"  onchange="loadFile(event)"  name="photo" id="image" style="display: none;">
                                    </label>
                                  </div>
                                </div>

   
                               
                          </div>
                  </div>
                </div>
              </div>
              </div>


               <div class="col-md-12">
                        <div class="col-md-2">
                          <div class="form-group">
                            <input type="number" class="form-control" placeholder="Zip *" name="Zip"    required/>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                          
<select class="form-control" name="Country"   required> 
<option value="USA">United States</option> 
</select>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <input type="text" class="form-control" placeholder="County" name="County"    />
                          </div>
                        </div>

                            <div  class="col-md-4 ">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Address Type" name="AddressType"    />
                              </div>
                            </div>

                             <div class="col-md-6">
                              <div class="form-group">
                                <input type="number" class="form-control" placeholder="Phone-1 *" name="Phone1"   required />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <select required class="form-control" name="Phone1Type">
                                  <option value="Home">Home</option>
                                  <option value="Work">Work</option>
                                  <option value="Mobile">Mobile</option>
                                  <option value="Others">Others</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group">
                              <select class="form-control" id="PhoneCarrier" name="PhoneCarrier" required>
                                <option value="">-- Select Phone Carrier --</option>
                                <option value="gocbw.com">Cincinnati &amp; Ohio,Cincinnati Bell</option>
                                <option value="sms.alltelwireless.com">Alltel (Allied Wireless)</option>
                                <option value="mms.att.net">AT&amp;T Mobility (formerly Cingular)</option>
                                <option value="myboostmobile.com">Boost Mobile</option>
                                <option value="mms.gocbw.com">Cincinnati Bell</option>
                                <option value="mms.cricketwireless.net">Cricke</option>
                                <option value="mymetropcs.com">MetroPCS</option>
                                <option value="vtext.com">Page Plus Cellular(Verizon MVNO)</option>
                                <option value="smtext.com">Simple Mobile</option>
                                <option value="mms.att.net">Straight Talk</option>
                                <option value="pm.sprint.com">Sprint</option>
                                <option value="tmomail.net">T-Mobile</option>
                                <option value="mms.uscc.net">US Cellular</option>
                                <option value="vzwpix.com">Verizon Wireless</option>
                                <option value="vmpix.com">Virgin Mobile</option>
                                <option value="mmst5.tracfone.com">Puerto Rico, and the US Virgin Islands,TracFone (prepaid)</option>
                              </select>
                            </div>
                          </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="number" class="form-control" placeholder="Phone-2" name="Phone2"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <select  class="form-control" name="Phone2Type">
                                  <option value="">-- Select Phone 2 Type --</option>
                                  <option value="Home">Home</option>
                                  <option value="Work">Work</option>
                                  <option value="Mobile">Mobile</option>
                                  <option value="Others">Others</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="number" class="form-control" placeholder="Phone-3" name="Phone3"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                               <select  class="form-control" name="Phone3Type">
                                  <option value="">-- Select Phone 3 Type --</option>
                                  <option value="Work">Work</option>
                                  <option value="Mobile">Mobile</option>
                                  <option value="Others">Others</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email Address" name="EMailAddress"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Alternate Address-1" name="AlternateAddress1"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Alternate Address-2" name="AlternateAddress2"    />
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Alternate City" name="AlternateCity"    />
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Alternate State" name="AlternateState"    />
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="number" class="form-control" placeholder="Alternate Zip" name="AlternateZip"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                               <input type="text" class="form-control" placeholder="Alternate County" name="AlternateCounty" />

                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">

<select  class="form-control" name="AlternateCountry" > 
<option value="" selected="selected"  >-- Select Alternate Country -- </option> 
<option value="USA">United States</option> 
</select>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Alternate Address Type" name="AlternateAddressType"    />
                              </div>
                            </div>


                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="School Name" name="SchoolName"    />
                              </div>
                            </div>

                            <div class="col-md-6 ">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="SSN" name="SSN"    />
                              </div>
                            </div>

                            <div class="col-md-6 ">
                              <div class="form-group">
                               <br><br>
                              </div>
                            </div>

                            <div class="col-md-6 ">
                              <div class="form-group">
                                <input type="date" class="form-control" placeholder="Birthdate *" name="Birthdate" />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="date" class="form-control" placeholder="DeathDate" name="DeathDate"    />
                              </div>
                            </div>



                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="number" class="form-control" placeholder="Referred By PatientId" name="ReferredByPatientId"    />
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <select class="form-control" name="PatientSameAsGuarantor">
                                  <option value="">-- Patient Same As Guarantor --</option>
                                  <option value="1">Yes</option>
                                  <option value="0">No</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Medical Record Number" name="MedicalRecordNumber"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="number" class="form-control" placeholder="Visit Document No." name="visdocnum"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="SSDID" name="SSDID"    />
                              </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group">
                              <textarea class="form-control" placeholder="Profile Notes" id="ProfileNotes" name="ProfileNotes"   ></textarea>
                            </div>
                            </div>


                            <div class="col-md-12">
                            <div class="form-group">
                            <textarea class="form-control" placeholder="Alert Notes" id="AlertNotes" name="AlertNotes"  ></textarea>
                            </div>
                            </div>


                            <div class="col-md-12">
                            <div class="form-group">
                              <textarea class="form-control" placeholder="Appointment Notes" id="AppointmentNotes" name="AppointmentNotes"   ></textarea>
                            </div>
                            </div>


                            <div class="col-md-12">
                            <div class="form-group">
                              <textarea class="form-control" placeholder="Billing Notes" id="BillingNotes" name="BillingNotes"   ></textarea>
                            </div>
                            </div>


                            <div class="col-md-12">
                            <div class="form-group">
                              <textarea class="form-control" placeholder="Reg Note" id="RegNote" name="RegNote"   ></textarea>
                            </div>
                            </div>

                             <div class="col-md-12">
                                  <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-green" name="btn-signup">Create Patient </button>
                                  </div>
                                </div>


                      </div>



            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop



