
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

@yield('title')

    <link rel="stylesheet" type="text/css" href="{{ url('/css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('/css/style2.css')}}">
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

     <script type="text/javascript">
        setTimeout(fade_out, 1000);
            function fade_out() {
              $("#alertmsg").fadeOut().empty();
            }
    </script>
 @yield('header-script')
</head>
<body>

<nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                     <a>
                         <img class="logo" src="{{ url('images/logo.png')}}" alt="HHM Logo">
                      </a>
            </div>

             <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-left">
                    
                        @yield('menu') 
                   
                </ul>

                 <ul class="nav navbar-nav navbar-right">
                        @if (Auth::user())
                        <li class="left-bar">
                            <img src="{{Auth::user()->image}}" class="img-responisve img-circle profile-nav" alt="">   
                        </li>
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->username }} <span class="caret"></span>
                            <div>{{Auth::user()->hospital }}</div>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                            <li><a href="account.php">Account Settings </a></li>
                                <li><a href="{{ route('logout')}}">Logout</a></li>
                            </ul>
                        </li>
                        @else 
                            <li class="left-bar"><a href="login">Login</a></li>
                            <li class="left-bar"><a href="login">Register</a></li>
                        @endif


                   
                </ul>
            </div>
        </div>
    </nav>

<br><br>


@yield('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>    
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>  


</body>
</html>





