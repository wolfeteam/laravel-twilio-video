@extends('layouts.public')

@section ('title')
    <title>Telemed | Call Patient</title>
@stop

<head>
    
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
  
    <meta name="description" content="Audio+Video+Screen Sharing using RTCMultiConnection" />
    <meta name="keywords" content="WebRTC,RTCMultiConnection,Demos,Experiments,Samples,Examples" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        * {
            word-wrap:break-word;
        }
        video {
            object-fit: fill;
            width: 30%;
        }
        button,
        input,
        select {
            font-weight: normal;
            padding: 2px 4px;
            text-decoration: none;
            display: inline-block;
            text-shadow: none;
            font-size: 16px;
            outline: none;
        }
        .make-center {
            text-align: center;
            padding: 5px 10px;
        }
        img, input, textarea {
          max-width: 100%
        }
        @media all and (max-width: 500px) {
            .fork-left, .fork-right, .github-stargazers {
                display: none;
            }
        }
        input{
            margin: auto;
            width: 50% ;
            text-align: center;
        }
        .chat-output{
            font-family: arial;
            font-size:18px;
            text-align: left;
        }
    </style>
</head>


<script type="text/javascript">
/*Script for download Chat andd page */
    function download(){
    var a = document.body.appendChild(
        document.createElement("a")
    );
    a.download = "export.html";
    a.href = "data:text/html," + document.getElementById("chat-container").innerHTML; // Grab the HTML
    a.click(); // Trigger a click on the element
}
</script>


<script type="text/javascript">
/*Get Room id in send notification Section */
  function getlink(){var bla = window.location.href+'?roomid='+$('#room-id').val();
  $('#room-id1').val(bla);
  $('#room-id2').val(bla);  
}
</script>


<script type="text/javascript">
   

    function getpatientdata(){
    if ($("#Selectpatient").val() != ''){   
        $.ajax({
           type:'post',
            url:'ajaxgetpatient',
            data: {'patientid':$("#Selectpatient").val(), '_token': $('input[name=_token]').val()},
            dataType:'html',

            success:function(response){
                var filesselector = '<select  name="patientfilesselector" class="form-control" id="ShareSelect"> '+response+'</select><br>';
               $("#patientfiles").html(filesselector);
               getcallid();
            }                
        })
        }
        else {
            $("#patientfiles").html('')
        }
    }



    function getcallid(){
    if ($("#Selectpatient").val() != ''){   
        $.ajax({
           type:'post',
            url:'ajaxgetcallid',
            data: {'patientid':$("#Selectpatient").val(), '_token': $('input[name=_token]').val()},
            dataType:'html',

            success:function(response){
               $("#sendNotModel").html(response);
               $("#sendNot").prop('disabled', false);
               document.getElementById('sharebtn').disabled = false;
            }  
               })
            }
    else {
            $("#sendNotModel").html('')
            $("#sendNot").prop('disabled',true);

        }

        }

</script>

@section('menu')
@if (Auth::user())
<li><a href="home" >HOME</a></li>
<li ><a href="viewpatient">PATIENTS</a></li>
<li class="active"><a href="callpatient">CALL</a></li>
<li ><a href="setup">SETUP</a></li>
@endif
@stop


@section ('content')
<div id="patientfiles1"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <ul class="list-group text-right">
                <li class="list-group-item "><a href="#" class="left-nav-active">Make Call</a></li>
              
                {{-- <li class="list-group-item"><a href="">Edit Patient Data</a></li> --}}
            </ul>    
            
            <div style="padding: 10px">
                <h4 class="text text-success">Appointments</h4>
                <button id="2232" class="selectPatient">create</button>
                <a data-target="#submitModal" data-toggle="modal" class="MainNavText" id="MainNavHelp">Select</a>

                <div id="appointment">
                    
                    @if(count($appointment) > 0)
                    <div class="card" style="width: 100%;">
                        <hr>
                        <div class="card-body">
                          <h4 class="card-title">{{ $appointment['patientname'] }}</h4>
                          <h5 class="card-subtitle mb-2 text-muted"><span class="text text-primary">Email:</span> {{ $appointment['patientemail'] }}</h5>
                          <h5 class="card-subtitle mb-2 text-muted"><span class="text text-primary">Phone:</span> {{ $appointment['mobilephone'] }}</h5>
                          <h5 class="card-subtitle mb-2 text-muted"><span class="text text-primary">DateTime:</span> {{ $appointment['appointment_datetime'] }} </h5>
                          <!--<p class="card-text"> </p>-->
                          <div>
                            <input type="hidden" value="{{ $appointment['patientid'] }}">
                            <button class="select-for-call btn btn-info">select me</button>
                          </div>
                        </div>
                      </div>
                    @endif
                    
                </div>
            </div>

        </div>


 <div class=" col-md-9 text-center" style="overflow-x:auto;">
        <section class="experiment">
            <div class="make-center">

        @if (session('status'))
    <div id="alertmsg" class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
                 
         <div class="row">
              
                <div class="col-md-7">
                    <div class="filesection chat-output"> 
                        <div class="file-container" id="file-container">
                            <p>Connect with Dr.Sam via TeleHealth. Click the link.</p>
                            <a href="" >link</a> to start and enter room 112.
                        </div>
                    </div> <br>
                    <table>
                        <tbody>
                            @if(count($appointment) > 0)
                            <tr>
                                <td  width="85%">
                                    <input class="form-control" type="text" id="" placeholder="Enter Text Chat" value=" {{ $appointment['patientemail'] }}" />
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                     @if (Auth::user())
                                        <button  class="btn btn-primary btn-cus container-fluid">Send Email</button>
                                    @endif
                                </td>
                            </tr>
                       
                            <tr>
                                <td  width="85%">
                                    <input class="form-control" type="text" id="" placeholder="Enter Text Chat" value="{{ $appointment['mobilephone'] }}" />
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                     @if (Auth::user())
                                        <button  class="btn btn-primary btn-cus container-fluid">Send Sms</button>
                                    @endif
                                </td>
                            </tr>
                            @endif
                         </tbody>    
                    </table>

                </div>
                 <br>
                </div>
            </div>
           
        </section>
     </div>    

</div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="submitModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Video Room</h4>
        </div>
         <form method="post" action="{{route('chat.createRoom')}}" class="horizontal">
              {{csrf_field()}}
            <div class="modal-body">
                    <div class="form-group">
                      <label class="">Room Name</label>
                      <input type="text" name="roomName" />
                      <input type="text" name="appointmentId" value="aaa" />
                  </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-success">Create</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
      
    </div>
  </div>
  
@if (Auth::user() && isset($patient))
<!-- Modal -->
<div id="sendNotModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div style="text-align:center;padding:15px;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color:#337ab7">Send patient telemedicine link</h4>
            <h5 class="modal-title" style="color:#337ab7">Click inside text to paste room url</h5>
        </div>
      
        <div class="col-md-3 text-right" style="border-right:2px solid #337ab7;padding-right: 0px;">
            <div class="modal-body">
                <div class="row">
                    <p style="padding-right: 12px;color:#337ab7">Select a room</p>
                    <div>
                        <p style="background-color:#337ab7;padding-right: 20px;color:white">Room1</p>
                        <p style="padding-right: 20px;color:#337ab7">Room2</p>
                        <p style="padding-right: 20px;color:#337ab7">Room3</p>
                        <p style="padding-right: 20px;color:#337ab7">Room4</p>
                        <p style="padding-right: 20px;color:#337ab7">Room5</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 ml-auto">
            <div class="modal-body">
                <div class="row">              
                    <form target="_blank" method="post" action="sendemail">
                        <div class="form-group">
                            <p style="color:#337ab7">Send link via email</p>
                            <input name="message" id="room-id1" onclick="return getlink();" value="" class="form-control">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="name" value="Patient System">
                            <input type="hidden" name="mail" value="{{$patient->EMailAddress}}">
                            <input type="hidden" name="subject" value="Chat-Request">
                        </div>
                        <button style="float:right" type="submit" class="btn btn-primary btn-cus">Send Email</button>
                    </form>
                </div>
                <div class="row">
                    <form target="_blank" method="post" action="sendsms">
                        <div class="form-group">
                            <p style="color:#337ab7">Send link via sms</p>
                            <input name="message" id="room-id2" onclick="return getlink();" value="" class="form-control">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="name" value="Patient System">
                            <input type="hidden" name="mobile" value="{{$patient->Phone1}}">
                            <input type="hidden" name="PhoneCarrier" value="{{$patient->PhoneCarrier}}">
                            <input type="hidden" name="subject" value="Chat-Request">
                        </div>
                        <button style="float:right" type="submit" class="btn btn-primary btn-cus">Send SMS</button>
                    </form>
                </div>
            </div>
        </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>

  </div>
</div>
@endif


        <script src="https://rtcmulticonnection.herokuapp.com/dist/RTCMultiConnection.min.js"></script>
        <script src="https://rtcmulticonnection.herokuapp.com/dev/globals.js"></script>
        <script src="https://rtcmulticonnection.herokuapp.com/socket.io/socket.io.js"></script>
        <script src="https://cdn.webrtc-experiment.com:443/FileBufferReader.js"></script>
        <!-- custom layout for HTML5 audio/video elements -->
        <script src="https://cdn.webrtc-experiment.com/getMediaElement.js"></script>
        <!-- capture screen from any HTTPs domain! -->
        <script src="https://cdn.webrtc-experiment.com:443/getScreenId.js"></script>
        <!--File Of Chat-->
        <script src="js/chat.js" ></script>

            @if(Auth::user())
            <script type="text/javascript">
            /* Sharing Patient Files */
              document.getElementById('sharebtn').onclick = function() {    
                         
                var file=document.getElementById('ShareSelect').value;

            /*Check file exxtensions*/
            var extension = file.substr( (file.lastIndexOf('.') +1) );
                switch(extension) {
                    case 'jpg':
                    case 'png':
                    case 'gif':
                        var link ='Me : <img src="'+file+'">' ;
                        connection.send(link);  
                        appendDIV(link);  
                    break;                         
                    case 'zip':
                    case 'rar':
                        var link ='Me : <a href="'+file+'">'+file+'</a>' ;  
                        connection.send(link);  
                        appendDIV(link);                 
                    break;
                    case 'pdf':
                         var link ='<iframe style="resize:both;" src="'+file+'" />'
                        connection.send(link);  
                        appendDIV(link);      
                    break;
                    case 'mpg':
                    case 'MPG':
                    case 'mp4':
                    case 'MP4':
                    case 'ogg':
                    case 'rm':
                    case 'WMV':
                    case 'wmv':
                    case 'AVI':
                    case 'avi':
                    case 'ASF':
                    case 'asf':
                    case 'MOV':
                    case 'mov':
                    case 'FLV':
                    case 'flv':
                    case 'SWF':
                    case 'swf':
                        var link ='Me :<video controls style="width:100%"><source src="'+file+'" type="video/mp4"></video>';
                        connection.send(link);  
                        appendDIV(link);
                    break;
                    case 'txt':
                        jQuery.get(file, function(data) {
                        connection.send(data);  
                        appendDIV(data);      
                        });
                    break;
                   default:
                    var link ='Me : <a href="'+file+'">'+file+'</a>' ;
                    connection.send(link);  
                    appendDIV(link);            
                }

            };  
            </script>
            @else 
            @endif 

            
            
            @if(Auth::user())
            <script type="text/javascript">
            document.getElementById('input-text-chat').onkeyup = function(e) {
                if (e.keyCode != 13) return;
                // removing trailing/leading whitespace
                this.value = this.value.replace(/^\s+|\s+$/g, '');
                if (!this.value.length) return;
                var divSelector=appendDIV;
                connection.send(this.value);
                appendDIV('Me : <b style="color:#19496f">'+this.value+'</b>');
                this.value = '';
            };
            </script>
            @else 
             <script type="text/javascript">
            document.getElementById('input-text-chat').onkeyup = function(e) {
                if (e.keyCode != 13) return;
                // removing trailing/leading whitespace
                this.value = this.value.replace(/^\s+|\s+$/g, '');
                if (!this.value.length) return;
                connection.send(this.value);
                appendDIV('Patient : <b style="color:#19496f">: '+this.value+'</b>');
                this.value = '';
            };
            </script>
            @endif

        <script>
             $("button.slt-for-call").click(function(){
                    $(".slt-for-call").prop('disabled', false);
                    $(this).prop('disabled', true);
                    var patientId = $(this).parent().children('input').val();
                    $("#Selectpatient").val(patientId);
                    getpatientdata();
                });
            window.useThisGithubPath = 'muaz-khan/RTCMultiConnection';
        </script>
        <script src="https://cdn.webrtc-experiment.com/commits.js" async></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.4.3.min.js" ></script>
        <script>
            $(document).on('click', '.selectPatient', function() {
                $
            })
        </script>
@stop



