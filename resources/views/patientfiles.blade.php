@extends('app')

@section ('title')
    <title>Telemed | Add Patient</title>
@stop

@section('menu')
<li><a href="  {{ url('home')}}" >HOME</a></li>
<li class="active"><a href="{{ url('viewpatient')}} ">PATIENTS</a></li>
<li><a href="{{ url('callpatient')}} ">CALL</a></li>
<li><a  href="{{url('setup')}}">SETUP</a></li>
@stop


@section ('content')


<div class="container-fluid"> 
  <div class="row">
    <div class="col-md-3">
      <ul class="list-group text-right">
        <li class="list-group-item "><a href="{{ url('viewpatient')}} " >View / Edit Patients</a></li>
        <li class="list-group-item"><a href="{{ url('addpatientpage')}}" >Add Patient</a></li>
        <!--<li class="list-group-item" class="left-nav-active"><a href="">Edit Patient Data</a></li>-->
      </ul>
    </div>
    <div class="col-md-7 lft-part">
      <div class="row">
        <div class="col-md-12">
           
            <h2 class="form-signin-heading">Patient Details</h2>
     <hr />
      @if (session('status'))
                      <div id="alertmsg" class="alert alert-success">
                          {{ session('status') }}
                      </div>
                      @endif
            <table class="table table-bordered table-responsive">
              <tbody>
              <tr>
                <td><b>Prefix</b></td><td>{{$patient->Prefix}}</td>
                <td><b>F-name</b></td><td>{{$patient->First}}</td>
                <td class="text-center" colspan="2" rowspan="5"> <img width="200px" src="{{url('')}}/{{$patient->Picture}}"> </td>
              </tr>

              <tr>
                 <td><b>M-name</b></td><td>{{$patient->Middle}}</td>
                <td><b>L-name</b></td><td>{{$patient->Last}}</td>
                
              </tr>

              <tr>
                <td><b>Suffix</b></td><td>{{$patient->Suffix}}</td>
                <td><b>Nickname</b></td><td>{{$patient->Nickname}}</td>
               
              </tr>

              <tr>
                 <td><b>Address1</b></td><td>{{$patient->Address1}}</td>
                <td><b>Address2</b></td><td>{{$patient->Address2}}</td>
                
              </tr>

              <tr>
                <td><b>City</b></td><td>{{$patient->City}}</td>
                <td><b>State</b></td><td>{{$patient->State}}</td>
              </tr>

              <tr>
                <td><b>Zip</b></td><td>{{$patient->Zip}}</td>
                <td><b>Country</b></td><td>{{$patient->Country}}</td>
                <td><b>AddressType</b></td><td>{{$patient->AddressType}}</td>
              </tr>

              <tr>
                <td><b>County</b></td><td>{{$patient->County}}</td>
                <td><b>Phone1</b></td><td>{{$patient->Phone1}}</td>
                <td><b>Phone1Type</b></td><td >{{$patient->Phone1Type}}</td>
              </tr>

              <tr>
                <td><b>Phone2</b></td><td>{{$patient->Phone2}}</td>
                <td><b>Phone2Type</b></td><td colspan="3">{{$patient->Phone2Type}}</td>
               
              </tr>

              <tr>
                 <td><b>Phone3</b></td><td>{{$patient->Phone3}}</td>
                 <td><b>Phone3Type</b></td><td colspan="3" >{{$patient->Phone3Type}}</td>
              </tr>

              <tr>
                <td><b>EMailAddress</b></td><td colspan="5">{{$patient->EMailAddress}}</td>
              </tr>

              <tr>
                <td><b>AlternateAddress1</b></td><td>{{$patient->AlternateAddress1}}</td>
                <td><b>AlternateAddress2</b></td><td >{{$patient->AlternateAddress2}}</td>
                <td><b>AlternateCity</b></td><td>{{$patient->AlternateCity}}</td>
              </tr>

              <tr>
                <td><b>AlternateState</b></td><td >{{$patient->AlternateState}}</td>
                <td><b>AlternateZip</b></td><td>{{$patient->AlternateZip}}</td>
                <td><b>AlternateCounty</b></td><td >{{$patient->AlternateCounty}}</td>
              </tr>

              <tr>
                <td><b>AlternateCountry</b></td><td>{{$patient->AlternateCountry}}</td>
                <td><b>AlternateAddressType</b></td><td >{{$patient->AlternateAddressType}}</td>
                <td><b>SchoolName</b></td><td>{{$patient->SchoolName}}</td>
              </tr>

              <tr>
                <td><b>SSN</b></td><td >{{$patient->SSN}}</td>
                <td><b>Birthdate</b></td><td>{{$patient->Birthdate}}</td>
                <td><b>DeathDate</b></td><td>{{$patient->DeathDate}}</td>
              </tr>

              <tr>
                <td><b>Sex</b></td><td>{{$patient->Sex}}</td>
                <td><b>ReferredByPatientId</b></td><td >{{$patient->ReferredByPatientId}}</td>
                <td><b>PatientSameAsGuarantor</b></td><td>@if($patient->PatientSameAsGuarantor==1) Yes @else No @endif </td>
              </tr>

               <tr>
                <td><b>MedicalRecordNumber</b></td><td >{{$patient->MedicalRecordNumber}}</td>
                <td><b>Visit document num</b></td><td>{{$patient->visdocnum}}</td>
                <td><b>SSDID</b></td><td >{{$patient->SSDID}}</td>
              </tr>

              <tr>
                 <td><b>ProfileNotes</b></td><td colspan="5">{{$patient->ProfileNotes}}</td>
              </tr>

              <tr>
                 <td><b>AlertNotes</b></td><td colspan="5">{{$patient->AlertNotes}}</td>
              </tr>

              <tr>
                <td><b>AppointmentNotes</b></td><td colspan="5">{{$patient->AppointmentNotes}}</td>
              </tr>

              <tr>
                <td><b>BillingNotes</b></td><td  colspan="5">{{$patient->BillingNotes}}</td>
              </tr>

              <tr>
                <td><b>RegNote</b></td><td  colspan="5" >{{$patient->RegNote}}</td>
              </tr>

              <tr>
                <td><b>Created</b></td><td >{{$patient->Created}}</td>
                <td><b>LastModified</b></td><td >{{$patient->LastModified}}</td>
              </tr>
            </tbody>
            </table>

     <hr />

      <form action="{{url('uppatientfiles')}}" class="form-signin" id="register-form" method="post" enctype="multipart/form-data">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="patientid" value="{{@$patient->PatientProfileId}}">

            <div class="col-md-6 ll-lft">
 
                        <div class="row">
            <div class="row">
              
                <div class="col-md-12">
                    <div class="form-group">
                      <label>Data Upload Type</label>
                     </div>
                  </div>
                  <div class="col-md-12 text-right rr">
                    <div class="form-group">
                      <!--<span>
                      Image <input type="radio" class="form-control1" name="RecevedTextMessage" value="Yes" >
                      </span>--> 
                      <span>Image</span><input class="custom-radio" type="radio" name="Type" id="image" checked="checked" value="Image"><label for="image"> <span>Image</span></label>
                       </div>
                  </div>
                  <div class="col-md-12 text-right rr">
                    <div class="form-group">
                     <!-- <span>
                      Video <input type="radio" class="form-control1" name="RecevedTextMessage" value="Yes" >
                      </span>-->
                      <span>Video</span><input class="custom-radio" type="radio" name="Type" id="video" value="Video"><label for="video"><span> Video</span></label>
                        </div>
                  </div>
                  <div class="col-md-12 text-right rr">
                    <div class="form-group">
                     <!-- <span>
                      Document <input type="radio" class="form-control1" name="RecevedTextMessage" value="Yes" >
                      </span> -->
                      <span>Document</span><input class="custom-radio" type="radio" name="Type" id="document" value="Document"><label for="document"> <span>Document</span></label> </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                    <br/>
                     <label class="btn btn-primary btn-cus"> Upload Documents
                            <input type="file" id="file" name="file" style="display: none;">
                          </label> </div>

                            <div class="form-group"> 
                          <!--<input type="file" name="image" id="image">-->
                         
                           <button onclick="return chkfile()" type="submit" class="btn btn-default btn-green" name="btn-submit"> Submit Documents</button>
                     
                        </div>
                  </div>
                  
                  
                  
                  
                  
                  <div class="col-md-12">
                  
                  </div>
    
                 
            </div>
            </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12 pull-right text-right">
                    <div class="row">


                      <div class="col-md-12">
                        
                            <div class="form-group">
                  <div class="patient-docu">

                    <h5>Patient Document</h5>
                      {{-- @foreach (@$patientfiles as $patientfile)

                                <p>

                                <span>{{@$patientfile->Filename}} - {{@$patientfile->Type}} - {{@$patientfile->created_at}}</span> 

                                <a href="../{{@$patientfile->Filelink}}" target="_blank" class="crossbtn"><span class="glyphicon glyphicon-edit"></span></a>

                                <a href="../deletefile/{{@$patientfile->id}}" class="crossbtn" onClick="return confirm('Are you sure you want to delete this item?');">
                                 <span class="glyphicon glyphicon-remove"></span>
                               
                                </a>


                                </p>
                      @endforeach --}}
                        
                                        </div>
                                        </div>
                       
                      </div>
                    </div>
                  </div>
                </div>
              </div>
      </form>
      </div>
    </div>


                     


  </div>
</div>

@stop

@section ('script')
<script type="text/javascript">
  function chkfile(){
    if(document.getElementById("file").files.length == 0 ){
      alert('please select patient document and upload it');
     return false;
    }
    else{
      return true;
    }
  }
</script>
@stop



