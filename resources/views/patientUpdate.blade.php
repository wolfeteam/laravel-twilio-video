@extends('app')

@section ('title')
    <title>Telemed | Add Patient</title>
@stop

@section('menu')
<li><a href="  {{ url('home')}}" >HOME</a></li>
<li class="active"><a href="{{ url('viewpatient')}} ">PATIENTS</a></li>
<li><a href="{{ url('callpatient')}} ">CALL</a></li>
<li><a  href="{{url('setup')}}">SETUP</a></li>
@stop


@section ('content')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

<div class="container-fluid"> 
  <div class="row">
    <div class="col-md-3">
      <ul class="list-group text-right">
        <li class="list-group-item "><a href="{{ url('viewpatient')}} " >View / Edit Patients</a></li>
        <li class="list-group-item"><a href="{{ url('addpatientpage')}}" >Add Patient</a></li>
        <!--<li class="list-group-item" class="left-nav-active"><a href="">Edit Patient Data</a></li>-->
      </ul>
    </div>
    <div class="col-md-7 lft-part">
      <div class="row">
        <div class="col-md-12">
           
            <h2 class="form-signin-heading"><span class="text text-info small">Pending Telehealth<span> {{ $patient[0]['firstname'] }}  {{ $patient[0]['lastname'] }}</h2>
     <hr />
      @if (session('status'))
                      <div id="alertmsg" class="alert alert-success">
                          {{ session('status') }}
                      </div>
      @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="update-box">
                <h3>Setup Patient</h3>
                <br>
                <p>
                  Complete patient infomation to set them up for virtual calls.
                </p>
                <br>
                <div id='complete-setup' class="update-button">
                  Complete Patient Setup
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="update-box">
                <h3>Get Patient Approval</h3>
                <br>
                <p>
                  Connect with the patient to timestamp approval for the
                  telemedicine call.
                </p>
                <br>
                <div id='confirm-approval' class="update-button">
                  Confirm Patient Approval
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="update-box">
                <h3>Create Appointments</h3>
                <br>
                <p>
                  Create an appointment date to remind the patient of their
                  upcoming Telehealth appointment.
                </p>
                <br>
                <div id='set-app' class="update-button" data-id="{{ $patient[0]['patientid'] }}">
                  Set App with ({{ $patient[0]['firstname'] }})
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="update-box">
                <h3>Have TeleVideo meeting and take notes</h3>
                <br>
                <p>
                  Start Telehealth call.
                </p>
                <br>
                <div id='start-call' class="update-button">
                  Start Call with ({{ $patient[0]['firstname'] }})
                </div>
                <div>
                    <form action="{{ route('callpatient') }}" class="hidden" method="post" id="appointment_form">
                        {{ csrf_field() }}
                        <input id="appointmentid" name="appointmentid" value="">    
                        <input id="appointmenttypeid" name="appointmenttypeid" value="">    
                        <input id="departmentid" name="departmentid" value="">    
                        <input id="providerid" name="providerid" value="">    
                        <input id="patientid" name="patientid" value="">    
                        <input id="appointmenttype" name="appointmenttype" value="">    
                        <input id="appointment_datetime" name="appointment_datetime" value="">    
                        <input name="patientname" value="{{ $patient[0]['firstname'].' '.$patient[0]['lastname'] }}">    
                        <input name="patientemail" value="{{ $patient[0]['email']}}">    
                        <input name="mobilephone" value="{{ $patient[0]['mobilephone']}}">
                    </form>
                </div>
              </div>
            </div>

            </div>

          </div>
        
     <hr />
     </div>
    </div>

    
    <div class="modal fade" id="myModal-box1" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Setup Patient</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">FullName</h4>
              </div>
              <div class="col-lg-8">
                <h4>{{ $patient[0]['firstname']}} {{ $patient[0]['lastname'] }}</h4>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">EMailAddress</h4>
              </div>
              <div class="col-lg-8">
                <h4>{{ array_key_exists("email",$patient[0])?$patient[0]['email']:'' }}</h4>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">PhoneCarrier</h4>
              </div>
              <div class="col-lg-8">
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="21") ACS Wireless  @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="22") Alltel @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="24") AT&T Cingular @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="26") Boost (iDEN) @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="34") Cincinnati Bell @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="35") Cricket Communincations @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="81") MetroPCS @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="58") SaskTel Mobility @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="36") GCI Communications @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="38") Illinois Valley Cellular @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="37") Golden State Cellular @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="59") Telus Mobility @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="49") US Cellular @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="50") Verizon Wireless @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="60") Virgin Mobile @endif
                @if ((array_key_exists("mobilecarrierid",$patient[0])?$patient[0]['mobilecarrierid']:"")=="57") Rogers Wireless @endif
              
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">PhoneNumbers</h4>
              </div>
              <div class="col-lg-8">
                <h4>{{ array_key_exists("mobilephone",$patient[0])?$patient[0]['mobilephone']:"" }}</h4>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="progress hidden">
              <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-success" id="confirm-box1">Confirm</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="myModal-box2" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Get Patient  Approval</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="myquestion col-lg-12">
                  <p>my question:</p>
                  <h4 class="text text-info">1. Are you giving consent to discuss your medical via telemedicine?</h4>
                </div>
                <div class="answer col-lg-12">
                  <span>patient answer:</span>
                  <select id='answer1' name="answer1" class="form-control">
                    <option name="answer1" value="yes" @if((array_key_exists("notes",$patient[0])?strpos($patient[0]['notes'],"phone"):"") == true) selected @endif>Yes</option>
                    <option name="answer1" value="no" @if((array_key_exists("notes",$patient[0])?strpos($patient[0]['notes'],"yes"):"") != true) selected @endif>No</option>
                  </select>
                </div>
            </div>
            <div class="row">
                <div class="myquestion col-lg-12">
                  <p>my question:</p>
                  <h4 class="text text-info">2. What is the way they wish to have the appointment, phone, or computer?</h4>
                </div>
                <div class="answer col-lg-12">
                  <span>patient answer:</span>
                  <select id='answer2' name="answer2" class="form-control">
                    <option name="answer2" value="appointment">Appointment</option>
                    <option name="answer2" value="phone" @if((array_key_exists("notes",$patient[0])?strpos($patient[0]['notes'],"phone"):"") == true) selected @endif>Phone</option>
                    <option name="answer2" value="computer" @if((array_key_exists("notes",$patient[0])?strpos($patient[0]['notes'],"computer"):"") == true) selected @endif>Computer</option>
                  </select>
                </div>
            </div>
            
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Provider</h4>
              </div>
              <div class="col-lg-8">
                <select class="form-control" id="fproviders" name="fproviderid" required>                
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Department</h4>
              </div>
              <div class="col-lg-8">
                <select class="form-control" id="fdepartments" name="fdepartmentid" required>                
                </select>
              </div>
            </div>
            
          </div>
          <div class="modal-footer">
            <div class="progress hidden">
              <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-success" id="confirm-box2">Confirm</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="myModal-box3" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Create Appointments</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Full Name</h4>
              </div>
              <div class="col-lg-8">
                <h4 id="fullname">{{ $patient[0]['firstname'] }}  {{ $patient[0]['lastname'] }}</h4>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Email</h4>
              </div>
              <div class="col-lg-8">
                <h4 id="email">{{ array_key_exists("email",$patient[0])?$patient[0]['email']:'' }}</h4>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Phone Numbers</h4>
              </div>
              <div class="col-lg-8">
                <h4 id="phone">{{ array_key_exists("mobilephone",$patient[0])?$patient[0]['mobilephone']:"" }}</h4>
              </div>
            </div>
            <div class="row hidden">
              <div class="col-lg-4">
                <h4 class="text text-info">Provider</h4>
              </div>
              <div class="col-lg-8">
                <input class="form-control" id="providers" name="providerid" required />                
              </div>
            </div>
            <div class="row hidden">
              <div class="col-lg-4">
                <h4 class="text text-info">Department</h4>
              </div>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="departments" name="departmentid" />
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Patient Appointment Reasons</h4>
              </div>
              <div class="col-lg-8">
                <select class="form-control" id="patientappointmentreasons" name="patientappointmentreasons" required>                
                </select>
              </div>
            </div>
            
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Appointment Type</h4>
              </div>
              <div class="col-lg-8">
                <select class="form-control" id="appointmenttypes" name="appointmenttypes" required>                
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">DateTime</h4>
              </div>
              <div class="col-lg-8" style="padding-top: 10px">
                  <input type="text" id="datetime" name="datetime" class="form-date">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Message</h4>
              </div>
              <div class="col-lg-8" style="padding-top: 10px">
                <textarea id="message" style="width: 100%; height:100px; resize: none;"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="progress hidden">
              <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-success" id="confirm-box3">Confirm</button>
          </div>
        </div>
      </div>
    </div>

                     


  </div>
</div>

@stop

@section ('script')
<script type="text/javascript">

///init


////////////

///box1
  $("#complete-setup").click(function(){
    var isActive = false;
    if($(this).parent().attr('class') == "update-box active"){
      isActive = true;
    }
    if(!isActive){
      $("#myModal-box1").modal("show");
    }
  });

  $("#confirm-box1").click(function(){
    if($('.progress').attr('class') == "progress")return;
      @if((array_key_exists("email",$patient[0])?$patient[0]['email']:"") == "" || (array_key_exists("mobilephone",$patient[0])?$patient[0]['mobilephone']:"") == "")
        alert("Please set EMailAddress or PhoneCarrier!");
        window.open("{{ url('editpatientpage/'.$patient[0]['patientid'])}}", '_self');
        return;
      @endif
    $('.progress').removeClass('hidden');
    $.ajax({
      type:'POST',
      url:'{{ url('ajaxUpdateSetupPatient')}}',
      data:{'patientid': {{ $patient[0]['patientid'] }}, '_token': $('input[name=_token]').val()},
      success:function(data){
        $('.progress').addClass('hidden');
        $("#myModal-box1").modal("hide");
        $('#complete-setup').parent().addClass('active');
        var providers = JSON.parse(data).providers.providers;
        var departments = JSON.parse(data).departments.departments;
        var providerSelect = document.getElementById('fproviders');
        var departmentSelect = document.getElementById('fdepartments');
        for (var i = 0; i<=providers.length-1; i++){
            var opt = document.createElement('option');
            opt.value = providers[i].providerid ;
            opt.innerHTML = providers[i].firstname;
            providerSelect.appendChild(opt);
        }
        for (var i = 0; i<=departments.length-1; i++){
            var opt = document.createElement('option');
            opt.value = departments[i].departmentid ;
            opt.innerHTML = departments[i].name;
            departmentSelect.appendChild(opt);
        }
        // completeStatus = 1;
      }
    });
  });
  //box2
  $("#confirm-approval").click(function(){
    var isActive = false;
    if($(this).parent().attr('class') == "update-box active"){
      isActive = true;
    }
    if(!isActive){
      @if((array_key_exists("email",$patient[0])?$patient[0]['email']:"") != "" || (array_key_exists("mobilephone",$patient[0])?$patient[0]['mobilephone']:"") != "" )
        $("#myModal-box2").modal("show");
      @else
        alert("Please setup patient!");
      @endif
    }
  });

  $("#confirm-box2").click(function(){
    if($('.progress').attr('class') == "progress")return;
    $('.progress').removeClass('hidden');
    
    $.ajax({
      type:'POST',
      url:'{{ url('ajaxUpdateGetApproval')}}',
      data:{'patientid': {{ $patient[0]['patientid'] }},'answer1':$("#answer1").val(), 'answer2':$("#answer2").val(),'fdepartments':$("#fdepartments").val(), 'fproviders':$("#fproviders").val(), '_token': $('input[name=_token]').val()},
      success:function(data){
        console.log("DD",JSON.parse(data))
        var providers = JSON.parse(data).providers;
        var departments = JSON.parse(data).departments;
        var appointmenttypes = JSON.parse(data).appointmentTypes.appointmenttypes;
        var patientappointmentreasons = JSON.parse(data).patientappointmentreasons.patientappointmentreasons;
        
        $('#providers').val(providers);
        $('#departments').val(departments);
        var appointmenttypeSelect = document.getElementById('appointmenttypes');
        var patientappointmentreasonsSelect = document.getElementById('patientappointmentreasons');

        for (var i = 0; i<=patientappointmentreasons.length-1; i++){
            var opt = document.createElement('option');
            opt.value = patientappointmentreasons[i].reasonid ;
            opt.innerHTML = patientappointmentreasons[i].reason;
            patientappointmentreasonsSelect.appendChild(opt);
        }
        
        for (var i = 0; i<=appointmenttypes.length-1; i++){
            var opt = document.createElement('option');
            opt.value = appointmenttypes[i].appointmenttypeid ;
            opt.innerHTML = appointmenttypes[i].name;
            appointmenttypeSelect.appendChild(opt);
        }
        console.log("DATAAAAAA",JSON.parse(data))
        $('.progress').addClass('hidden');
        $("#myModal-box2").modal("hide");
        if(JSON.parse(data).answer1 == "yes"){
          $('#confirm-approval').parent().addClass('active');
          completeStatus = 2;
        }
      }
    });
  });
 
///box3
$("#set-app").click(function(){
    var isActive = false;
    if($(this).parent().attr('class') == "update-box active"){
      isActive = true;
    }
    if(!isActive){
      @if((array_key_exists("email",$patient[0])?$patient[0]['email']:"") != "" || (array_key_exists("mobilephone",$patient[0])?$patient[0]['mobilephone']:"") != "")
        $("#myModal-box3").modal("show");
      @else
        alert("Please get patient approval!");
      @endif
    }
  });
  
  $("#confirm-box3").click(function(){
      
     if($('#datetime').val() == null || $('#datetime').val() =="") {
         alert("Please select datetime");
         return;
     }
    if($('.progress').attr('class') == "progress")return;
    
    $('.progress').removeClass('hidden');
    $.ajax({
      type:'POST',
      url:'{{ url('ajaxUpdateCreateAppointment')}}',
      data:{
        'patientid': {{ $patient[0]['patientid'] }},
        'departmentid': $('#departments').val(),
        'providerid':$('#providers').val(),
        'note':$('#myModal-box3 #message').val(),
        'datetime':$('#datetime').val(),
        'patientappointmentreasons':$('#patientappointmentreasons').find(":selected").val(),
        'appointmenttypeid':$('#appointmenttypes').find(":selected").val(),
         '_token': $('input[name=_token]').val()},
      success:function(data){
        $('.progress').addClass('hidden');
        $("#myModal-box3").modal("hide");
        $('#set-app').parent().addClass('active');
        completeStatus = 3;
        //set values for the call page
        var result = JSON.parse(data);
        $('#appointmentid').val(result[0]['appointmentid'])
        $('#appointmenttypeid').val(result[0]['appointmenttypeid'])
        $('#departmentid').val(result[0]['departmentid'])
        $('#providerid').val(result[0]['providerid'])
        $('#patientid').val(result[0]['patientid'])
        $('#appointmenttype').val(result[0]['appointmenttype'])
        $('#appointment_datetime').val(result[0]['date']+' '+result[0]['starttime'])
        }
    });
  });

  
///box4
$("#start-call").click(function(){
    var isActive = false;
    if($(this).parent().attr('class') == "update-box active"){
      isActive = true;
    }
    if(!isActive){
      @if((array_key_exists("email",$patient[0])?$patient[0]['email']:"") != "" || (array_key_exists("mobilephone",$patient[0])?$patient[0]['mobilephone']:"") != "")
        $.ajax({
          type:'POST',
          url:'{{ url('ajaxUpdateHaveMeeting')}}',
          data:{'patientid': {{ $patient[0]['patientid'] }}, '_token': $('input[name=_token]').val()},
          success:function(data){
            $('#start-call').parent().addClass('active');
            completeStatus = 4;
          }
        });
        $("#appointment_form").submit();
        // window.open("{{ url('callpatient')}}", '_self');
      @else
        alert("Please create appointments!");
      @endif
    }
  });
</script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr('#datetime',{enableTime:true});
</script>
@stop



