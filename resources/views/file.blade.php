@extends('app')

@section('title')
<title>Telemed | Home</title>
@stop


@section('menu')
<li class="active"><a href="home" >HOME</a></li>
<li><a href="viewpatient">PATIENTS</a></li>
<li><a href="callpatient">CALL</a></li>
<li><a  href="setup">SETUP</a></li>
@stop


@section('content')
<br><br>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <ul class="list-group text-right">
                <li class="list-group-item "><a href="home" class="left-nav-active">Home</a></li>
             </ul>          
        </div>

        <div class="col-md-7">

		<form action="fileUpload" method="post" enctype="multipart/form-data">
		 <label for="fname">Upload:</label>
		  <input type="file" id="file" name="file"><br><br>
		  <input type="submit" value="Submit">
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		</form>
    </div>
</div>
</div>

@stop






