@extends('app')
@section ('title')
<title>Telemed | View Patients</title>
@stop
@section('menu')
<li><a href="{{URL('home')}}" >HOME</a></li>
<li class="active"><a href="viewpatient">PATIENTS</a></li>
<li><a href="callpatient">CALL</a></li>
<li><a  href="{{url('setup')}}">SETUP</a></li>
@stop
@section ('content')
<div class="container-fluid">
<div class="row">
   <div class="col-md-3">
        <ul class="list-group text-right">
            <li class="list-group-item "><a href="viewpatient" class="left-nav-active">View / Edit Patients</a></li>
            <li class="list-group-item"><a href="addpatientpage" >Add Patient</a></li>
            <!--<li class="list-group-item"><a href="">Edit Patient Data</a></li>-->
        </ul>
   </div>
   <div class="col-md-7">
      <div class="row">
         <div class="col-md-6">
            <form method="post" action="{{url('searchpatient')}}">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <div class="form-group input-group">                    
                  <input type="text" name="search"  class="form-control search" placeholder="Search..." >
                  <span class="input-group-btn"> 
                  <button  class="btn btn-default" type="submit">
                  <i class="fa fa-search"></i>
                  </button>
                  </span>
               </div>
            </form>
         </div>
      </div>
      
      @if (session('status'))
      <div id="alertmsg" class="alert alert-success">
         {{ session('status') }}
      </div>
      @endif
      <div class="row"  id="txtHint">
         @if (is_array($patients))
         @foreach ($patients as $patient)
         @if (is_array($patient))
         @foreach ($patient as $p)
         <div class="col-md-6">
            <div class="media mediapro">
                <div class="media-left">
                    <img src="{{url('images/noimage.jpg')}}" class="media-object" > 
                </div>
                <div class="media-body ">
                    <h4 class="media-heading">{{$p['firstname'] }} {{$p['lastname'] }} </h4>
                    <a href="{{url('deletepatient')}}/{{$p['patientid']}}" onClick="return confirm('Are you sure you want to delete this item?');" class="icon-btn hidden">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true hidden"></span>
                    </a>
                    <a href="editpatientpage/{{$p['patientid']}}" class="icon-btn hidden"> 
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

                    <a href="patientfiles/{{$p['patientid']}}" class="icon-btn hidden"> 
                         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    </a>
                    <a href="patientUpdate/{{$p['patientid']}}" class="icon-btn"> 
                        <!--<span class="glyphicon glyphicon-tint" aria-hidden="true"></span>-->
                        <img src="{{asset('images/Icon_ondemand-video.png')}}" />
                   </a>
                </div>
            </div>
            <hr>
         </div>
         @endforeach
         @endif
         @endforeach
         @endif
      </div>
   </div>
</div>

@stop